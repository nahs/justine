-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 09 2016 г., 12:57
-- Версия сервера: 5.1.68-community-log
-- Версия PHP: 5.4.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `justine`
--

-- --------------------------------------------------------

--
-- Структура таблицы `builder`
--

CREATE TABLE IF NOT EXISTS `builder` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `builder`
--

INSERT INTO `builder` (`id`, `name`, `adress`, `phone`) VALUES
(1, 'ООО "Рабочий"', 'ыа', '2134'),
(2, 'ООО "Дорогастрой"', 'цаф1111', '56'),
(3, 'ООО "Кабелетян"', 'ццц', '231434');

-- --------------------------------------------------------

--
-- Структура таблицы `builder_in_typeofwork`
--

CREATE TABLE IF NOT EXISTS `builder_in_typeofwork` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_builder` int(10) NOT NULL,
  `id_typeofwork` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Дамп данных таблицы `builder_in_typeofwork`
--

INSERT INTO `builder_in_typeofwork` (`id`, `id_builder`, `id_typeofwork`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 3, 3),
(4, 1, 4),
(5, 2, 5),
(6, 3, 6),
(8, 1, 8),
(9, 1, 8),
(10, 1, 8),
(11, 2, 9),
(12, 2, 10),
(13, 1, 11),
(14, 3, 12),
(15, 1, 13),
(16, 2, 14),
(17, 3, 15),
(18, 1, 16),
(19, 2, 17);

-- --------------------------------------------------------

--
-- Структура таблицы `contract`
--

CREATE TABLE IF NOT EXISTS `contract` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `number` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `id_contragent` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `sum_contract` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `contract`
--

INSERT INTO `contract` (`id`, `name`, `number`, `date`, `id_contragent`, `start_date`, `end_date`, `sum_contract`) VALUES
(1, 'Договор 1', 'СХС0001', '2013-04-10', 2, '2013-04-14', '2013-04-20', 123),
(2, 'Договор 2', 'СХС0002', '2013-04-11', 4, '2013-06-11', '2013-07-13', 45),
(3, 'wertwrt', '234', '2013-07-11', 5, '2013-07-18', '2013-08-23', 456);

-- --------------------------------------------------------

--
-- Структура таблицы `contragent`
--

CREATE TABLE IF NOT EXISTS `contragent` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `contragent`
--

INSERT INTO `contragent` (`id`, `name`, `adress`, `phone`) VALUES
(1, 'ООО "Застройщик"', 'цуа', '534343'),
(2, 'ООО "Клиент"', 'фцрацапр', '35434'),
(3, 'Хаббаров Равзат Рижаутович', 'цацыа', '354354354'),
(4, 'sdfsdf', 'sdfsdf', 'sdfsdf'),
(5, 'Человек', 'Гдето чтото', '555-55-55');

-- --------------------------------------------------------

--
-- Структура таблицы `contragent_in_object`
--

CREATE TABLE IF NOT EXISTS `contragent_in_object` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_contragent` int(10) NOT NULL,
  `id_object` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `contragent_in_object`
--

INSERT INTO `contragent_in_object` (`id`, `id_contragent`, `id_object`) VALUES
(1, 3, 1),
(6, 1, 7),
(14, 3, 15),
(16, 2, 17),
(17, 4, 0),
(18, 4, 0),
(19, 1, 0),
(20, 4, 0),
(21, 5, 0),
(22, 5, 0),
(24, 1, 19),
(25, 1, 20);

-- --------------------------------------------------------

--
-- Структура таблицы `doc_in_machineryfact`
--

CREATE TABLE IF NOT EXISTS `doc_in_machineryfact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_machineryfact` int(11) NOT NULL,
  `document` varchar(32) NOT NULL,
  `doc_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `doc_in_machineryfact`
--

INSERT INTO `doc_in_machineryfact` (`id`, `id_machineryfact`, `document`, `doc_name`) VALUES
(1, 1, 'c3dbb301fa05ce07ead8ba37fb0479ac', '03.jpg'),
(3, 1, '148ed382b0d35c5607a8449f631c6efb', '02.jpg'),
(4, 1, 'eabebd5a99484153a1d5925965f51f96', 'Animals_Birds_Duck_019397_.jpg'),
(13, 7, '833d064b37c180ab0455febd5a947b11', 'Wallpaper-46.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `doc_in_materialsfact`
--

CREATE TABLE IF NOT EXISTS `doc_in_materialsfact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_materialsfact` int(11) NOT NULL,
  `document` varchar(32) NOT NULL,
  `doc_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `doc_in_materialsfact`
--

INSERT INTO `doc_in_materialsfact` (`id`, `id_materialsfact`, `document`, `doc_name`) VALUES
(25, 8, '191efa56795e4d444b0064991928a599', '02.jpg'),
(27, 10, '6e84b7d1c1b3af9b42b6c161135a20d3', '24 июня 110.jpg'),
(28, 10, '3d545fb696507ee90d58e7ed6133549f', '1276415606_index.jpeg'),
(29, 10, '2d63ea278b14073a3fca868dfa6c2608', 'Nature_Flowers__004219_.jpg'),
(35, 11, '0e5815906d23c7b8cb1dd361c640820f', '02.jpg'),
(36, 11, 'd20621b0e7777e15728a63b6032162b2', 'Wallpaper-46.jpg'),
(37, 12, '35fba19156801a7f85743700ce90c3e4', '1282456052_cvetushiy_abrikos_192'),
(40, 14, '8fe937c07e9b7384c140900d634976d9', 'Wallpaper-46.jpg'),
(41, 16, 'b2c6b810939ca09e7e1a31627e34d2be', 'risunok_cheloveka_s_zontom_1280x');

-- --------------------------------------------------------

--
-- Структура таблицы `doc_in_workfact`
--

CREATE TABLE IF NOT EXISTS `doc_in_workfact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_workfact` int(11) NOT NULL,
  `document` varchar(32) NOT NULL,
  `doc_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `doc_in_workfact`
--

INSERT INTO `doc_in_workfact` (`id`, `id_workfact`, `document`, `doc_name`) VALUES
(6, 3, 'fdb8b932ca98b11e4f2fa41dbbda0f4b', '03.jpg'),
(8, 3, '5240ee4ae50db065a056d7c8869be127', '02.jpg'),
(9, 3, 'df08eca97835e8ba3f25e08427605d36', 'Animals_Birds_Duck_019397_.jpg'),
(10, 4, 'f6ac1eb38b952261f624beba4e8af147', 'Wallpaper-46.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `glava` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `group`
--

INSERT INTO `group` (`id`, `glava`, `name`) VALUES
(1, 'Глава 1', 'Подготовка территории строительства'),
(2, 'Глава 2', 'Основные объекты строительства'),
(3, 'Глава 3', 'Объекты подсобного и обслуживающего назначения'),
(4, 'Глава 4', 'Объекты энергетического хозяйства'),
(5, 'Глава 5', 'бъекты транспортного хозяйства и связи'),
(6, 'Глава 6', 'Наружные сети и сооружения водоснабжения, канализации, теплоснабжения и газоснабжения'),
(7, 'Глава 7', 'Благоустройство и озеленение территории'),
(8, 'Глава 8', 'Временные здания и соружения'),
(9, 'Глава 9', 'Прочие работы и затраты');

-- --------------------------------------------------------

--
-- Структура таблицы `machinerybid`
--

CREATE TABLE IF NOT EXISTS `machinerybid` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `measure` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `date` date NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `document` varchar(32) NOT NULL,
  `worktype` int(10) NOT NULL,
  `doc_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `machinerybid`
--

INSERT INTO `machinerybid` (`id`, `name`, `measure`, `amount`, `price`, `date`, `disclaimer`, `document`, `worktype`, `doc_name`) VALUES
(1, '111', '11', 11, 11, '2013-06-01', '1111', '315aa59a6f0bfc90e2773d9a14435a5f', 1, '02.jpg'),
(2, 'aaa', 'aa', 33, 33, '2013-07-05', '333', '043b764b6061d2c1b4d2af3064b5b280', 1, '01.jpg'),
(4, 'ret', 'r', 4, 4, '2013-07-04', '', '7f2e5d4518d7f6fbd0acedb96aa5a536', 1, 'Animals_Birds_Duck_019397_.jpg'),
(10, 'лорлрл', 'апра', 7, 6, '0000-00-00', '', 'c95c2da86b37bf46db0800f85e917e8b', 0, 'Autumn_01.jpg'),
(14, 'tryryrty', '6', 6, 6, '0000-00-00', '', '9e8bcac36ad0e02c7b83d07feddc5820', 0, '01.jpg'),
(16, 'jljkljkljkljklj', 'jkljkljklj', 87, 78, '0000-00-00', '', 'f09f3512798164fccfec0b0e7b10298c', 1, 'Wallpaper-46.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `machineryfact`
--

CREATE TABLE IF NOT EXISTS `machineryfact` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `measure` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `date` date NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `worktype` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `machineryfact`
--

INSERT INTO `machineryfact` (`id`, `name`, `measure`, `amount`, `price`, `date`, `disclaimer`, `worktype`) VALUES
(1, '111', '11', 11, 11, '2013-06-01', '111', 1),
(7, '6456456', '456456', 65, 65, '2013-07-20', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `machineryplan`
--

CREATE TABLE IF NOT EXISTS `machineryplan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `measure` varchar(25) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `worktype` int(10) NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `machineryplan`
--

INSERT INTO `machineryplan` (`id`, `name`, `measure`, `amount`, `price`, `worktype`, `disclaimer`, `date`) VALUES
(1, 'qweqwe', '12', 12, 12, 2, '12', '0000-00-00'),
(2, '32432', '12', 23, 12, 1, '', '0000-00-00'),
(3, '12', '12', 10, 5, 4, '', '0000-00-00'),
(4, 'werw', '1', 1, 1, 9, '', '2013-07-05');

-- --------------------------------------------------------

--
-- Структура таблицы `materialsbid`
--

CREATE TABLE IF NOT EXISTS `materialsbid` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `measure` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `date` date NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `document` varchar(32) NOT NULL,
  `worktype` int(10) NOT NULL,
  `doc_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Дамп данных таблицы `materialsbid`
--

INSERT INTO `materialsbid` (`id`, `name`, `measure`, `amount`, `price`, `date`, `disclaimer`, `document`, `worktype`, `doc_name`) VALUES
(45, 'wwwwwwwww', 'wwwwwwwww', 66, 66, '2013-05-01', 'wwwwwwww', 'bb864b7ec505f6dafe8a9597f0bb7095', 1, 'Nature_Flowers__004219_2758.jpg'),
(46, 'cccccccccc', 'ccccccccccc', 55, 55, '2013-05-01', '', '6ba961f11b2afd7e81a407e91c012243', 4, '017158.jpg'),
(47, 'eeeee', 'eeee', 23, 23, '0000-00-00', '', '9a186d86d1377ff723342089223dabed', 1, '024559.jpg'),
(50, '5', '', 0, 0, '0000-00-00', '', 'eeb19c420ff77c8ee49abcc76408f53e', 1, '24 июня 110.jpg'),
(51, 'zxzxzxzxzx', 'jjjj', 0, 0, '2013-05-09', 'duckbmbmn', '6b1ba86e9e0fcf8fef9e4c068810ab05', 1, 'Animals_Birds_Duck_019397_.jpg'),
(52, '11', '11', 11, 11, '0000-00-00', '', '76c49bccc62cc8de21ca89cba5feb2ec', 0, '03.jpg'),
(54, '111', '11', 11, 11, '2013-05-03', '', '01695db1aacdf893ad1b5e7c9bd337d0', 1, '02.jpg'),
(55, '11', '11', 11, 11, '2013-05-03', '', '521bb45ecdeb6de9c6bfe1dcf02abf05', 1, '02.jpg'),
(56, '11', '11', 11, 11, '2013-05-01', '', 'c9bf46ab621e86184d6ea9680a6a45d7', 1, '24 июня 110.jpg'),
(57, '11', '11', 11, 11, '2013-05-02', '', '5518caabdd3aac1d44291b14df9ec75c', 1, '24 июня 110.jpg'),
(59, 'sfgs', 'w', 12, 12, '2013-07-12', '', '', 14, '.'),
(60, 'hgjtyuey', 'jg', 67, 45, '0000-00-00', '', 'd066ee62c98201bfb4c7daf354ff00a0', 0, 'Autumn_01.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `materialsfact`
--

CREATE TABLE IF NOT EXISTS `materialsfact` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `measure` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `date` date NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `worktype` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `materialsfact`
--

INSERT INTO `materialsfact` (`id`, `name`, `measure`, `amount`, `price`, `date`, `disclaimer`, `worktype`) VALUES
(8, '11', '11', 11, 11, '2013-05-01', '', 1),
(9, '22', '22', 22, 22, '0000-00-00', '', 1),
(10, 'wer', 'wer', 14, 14, '2013-05-02', '', 1),
(11, 'sfgsfgsg111111111111', 'sdfgsgsdfg', 11, 11, '2013-05-02', '', 1),
(14, 'qwe', '11', 11, 11, '2013-06-08', '', 4),
(15, 'ффыфыфы', '13', 13, 13, '2013-07-18', '', 2),
(16, 'sdfs', '45', 45, 45, '2013-07-12', 'kljlkmbniur,tmnyoiubn,vm"dsper,mclcigh', 15),
(17, 'werg', '3', 3, 3, '2013-07-12', '', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `materialsplan`
--

CREATE TABLE IF NOT EXISTS `materialsplan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `measure` varchar(25) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `worktype` int(10) NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `materialsplan`
--

INSERT INTO `materialsplan` (`id`, `name`, `measure`, `amount`, `price`, `worktype`, `disclaimer`, `date`) VALUES
(1, 'песок', 'тонн', 10, 1, 1, 'привезти лучше сухой, так легче', '2013-01-01'),
(2, 'асфвальт', 'килограмм', 20, 1, 1, 'черный и гладкий', '2013-02-01'),
(3, '11', 'йц', 12, 3, 2, 'кйц', '0000-00-00'),
(4, 'Щебень', 'тонн', 3, 309, 3, 'мелкий', '0000-00-00'),
(5, 'Щебень 2', 'кг', 5, 10, 4, 'любой', '0000-00-00'),
(6, 'Щебень2', 'й', 12, 23, 5, 'цфукукаам', '0000-00-00'),
(7, 'цуфукц', 'ц', 2, 2, 5, 'ыфафыа', '0000-00-00'),
(8, '11111', 'qw', 12, 1, 9, 'asdfbgas', '0000-00-00'),
(9, '222222', '', 0, 0, 8, '', '0000-00-00'),
(10, '5', '', 0, 0, 7, '', '0000-00-00'),
(11, 'мост', '', 2, 2, 9, '', '0000-00-00'),
(12, 'ййййййййй', 'we', 5, 10, 9, '', '0000-00-00'),
(14, '2222', 'q', 12, 12, 11, '33333', '0000-00-00'),
(15, 'qerqr', '12', 30, 1, 1, '', '2013-03-01'),
(16, '123', '1', 1, 1, 9, '', '2013-07-12'),
(17, 'asd', 'a', 12, 12, 15, 'sdaadtgadfasgfgczsrftwsergcvxbrgser', '2013-07-04'),
(18, 'ffff', 'ff', 13, 13, 14, '', '2013-07-04'),
(19, 'fff1', '1', 45, 54, 16, '', '2013-07-12'),
(20, '4', '4', 40, 1, 1, '4', '2013-04-01');

-- --------------------------------------------------------

--
-- Структура таблицы `object`
--

CREATE TABLE IF NOT EXISTS `object` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `limit_sum` varchar(50) NOT NULL,
  `bailee` int(10) NOT NULL,
  `connection` enum('project','contract','rejection','') NOT NULL DEFAULT 'project',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `object`
--

INSERT INTO `object` (`id`, `name`, `start_date`, `end_date`, `limit_sum`, `bailee`, `connection`) VALUES
(1, 'Большой красивый дом', '2013-06-04', '2013-06-20', '105060000', 157, 'project'),
(7, 'Дом с привидениями', '2013-04-18', '2013-05-23', '15544888', 20, 'rejection'),
(15, 'Детский сад "Лодочка"', '2013-04-21', '2013-06-20', '12365454', 10, 'contract'),
(17, 'Новый дом', '2013-04-18', '2013-05-23', '12313123', 2, 'contract'),
(19, 'gfrty', '2013-07-12', '2013-08-06', '678', 4, 'contract'),
(20, '11', '2013-09-13', '2013-10-14', '12', 2, 'project');

-- --------------------------------------------------------

--
-- Структура таблицы `object_in_contract`
--

CREATE TABLE IF NOT EXISTS `object_in_contract` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_object` int(10) NOT NULL,
  `id_contract` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `object_in_contract`
--

INSERT INTO `object_in_contract` (`id`, `id_object`, `id_contract`) VALUES
(13, 15, 1),
(14, 17, 1),
(15, 0, 2),
(16, 0, 2),
(18, 19, 3),
(21, 17, 1),
(22, 17, 1),
(23, 17, 1),
(24, 17, 1),
(25, 15, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `object_in_rejection`
--

CREATE TABLE IF NOT EXISTS `object_in_rejection` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_object` int(10) NOT NULL,
  `id_rejection` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `object_in_rejection`
--

INSERT INTO `object_in_rejection` (`id`, `id_object`, `id_rejection`) VALUES
(4, 7, 1),
(5, 7, 1),
(6, 7, 1),
(7, 7, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `login` char(32) NOT NULL,
  `passwd` char(32) NOT NULL,
  `post` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `personal`
--

INSERT INTO `personal` (`id`, `name`, `login`, `passwd`, `post`, `department`) VALUES
(1, '111', '698d51a19d8a121ce581499d7b701668', '111', '', ''),
(2, 'Бушметов Александр Ваззапович', '8f9bfe9d1345237cb3b2b205864da075', '111', '', ''),
(4, 'aaaabb', 'b385531383eda6ff87544e7f4e4528b5', '111', 'aaaaa', 'aaaaaaaaa');

-- --------------------------------------------------------

--
-- Структура таблицы `rejection`
--

CREATE TABLE IF NOT EXISTS `rejection` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `number` varchar(50) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `rejection`
--

INSERT INTO `rejection` (`id`, `name`, `number`, `date`) VALUES
(1, 'Отказ 1', 'СХСо0001', '2013-04-11');

-- --------------------------------------------------------

--
-- Структура таблицы `typeofwork`
--

CREATE TABLE IF NOT EXISTS `typeofwork` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `group` int(1) NOT NULL,
  `id_object` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `typeofwork`
--

INSERT INTO `typeofwork` (`id`, `name`, `group`, `id_object`) VALUES
(1, 'Разборка асфальтобетонного покрытия тротуаров толщиной 12см', 1, 1),
(2, 'Какая-то работа', 2, 1),
(3, 'ываыаыа', 3, 1),
(4, 'ываыаыа', 1, 1),
(5, 'ЦУКАЕЦУКЕУЦК', 5, 1),
(6, 'уыпеукыпеук', 1, 1),
(8, 'Щипать травку', 7, 1),
(9, 'Положить асфальт', 1, 2),
(10, '1112313123', 7, 1),
(11, '1111', 2, 2),
(12, '53452353', 7, 1),
(13, 'qwe', 9, 15),
(14, 'asd', 1, 15),
(15, 'q11', 1, 18),
(16, 'fffff', 2, 15),
(17, 'dfgdf', 4, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `workbid`
--

CREATE TABLE IF NOT EXISTS `workbid` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `measure` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `date` date NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `document` varchar(32) NOT NULL,
  `worktype` int(10) NOT NULL,
  `doc_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `workbid`
--

INSERT INTO `workbid` (`id`, `name`, `measure`, `amount`, `price`, `date`, `disclaimer`, `document`, `worktype`, `doc_name`) VALUES
(3, '111', '111', 111, 111, '2013-06-01', '111ertet', 'cc9c7f5c947cd074eb747e262f51e601', 1, '02.jpg'),
(4, 'zx2', 'zx2', 7, 57, '2013-07-10', 'zx2 7 57', '66325db42fbbe808c74f003732c225ec', 4, 'Autumn_01.jpg'),
(6, 'zx3', 'zx3', 5, 5, '2013-07-04', 'zx3', '164dc9b50e79851ceceb5c339a7ce44e', 4, 'Wallpaper-46.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `workfact`
--

CREATE TABLE IF NOT EXISTS `workfact` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `measure` varchar(50) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `date` date NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `worktype` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `workfact`
--

INSERT INTO `workfact` (`id`, `name`, `measure`, `amount`, `price`, `date`, `disclaimer`, `worktype`) VALUES
(3, '111wer', '11', 11, 11, '2013-06-01', '111', 1),
(4, 'rtyry', '456', 765, 34, '2013-07-12', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `workplan`
--

CREATE TABLE IF NOT EXISTS `workplan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `measure` varchar(25) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` int(10) NOT NULL,
  `worktype` int(10) NOT NULL,
  `disclaimer` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `workplan`
--

INSERT INTO `workplan` (`id`, `name`, `measure`, `amount`, `price`, `worktype`, `disclaimer`, `date`) VALUES
(1, '23', '12', 12, 12, 4, '', '2013-07-18'),
(2, 'zx1', 'zx1', 56, 56, 4, 'zx1 56', '2013-07-04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
