﻿$(document).ready(function(){

//	роутер системы
//	@input 	c - имя вызываемого класса
//	@input 	m - имя вызываемого метода
//	@input 	d - передаваемые в метод данные(data)
//	@return	результат обработки данных(json) контроллера
//	@return	r.fn - имя вызываемой функции
//	@return	r.fndata - данные для вызываемой функции
//	@return	r.html - сгенерированный исходник страницы для вывода
function Router(c,m,d){
	$.ajax({
		url: 	'php/router.php',
		data:	({
					c:c,
					m:m,
					d:d
				}),
		dataType: 'json',
		success: function(r,s,x){
					//console.log(r);
					if(r.fn){
							var cb = $.Callbacks();
								cb.add(eval(r.fn));
								cb.fire(r.fndata);
								cb.remove(eval(r.fn));
						}
					$('content').fadeIn('slow').html(r.html);
					
				},
		beforeSend: function(x){
			$('#preloader').fadeIn('fast');
			$('content').children().fadeOut('fast');
		},
		complete: function(r,s){
			$.getScript('js/kickstart.js');
			$('#preloader').fadeOut('slow');
			
		}
	});
}

function refresh(d){
	$('section').append(d);
};

// Записывает в сесссию хеш пользователя
// @input d - md5(user)
function auth(d){
	sessionStorage.setItem('user', d);
	Router('ProjectPage','ProjectTable');
}

////////////////////////////      START PAGE "АВТОРИЗАЦИЯ"  ////////////////////////////////////////////////
// ЗАПУСК СТРАНИЦЫ АВТОРИЗАЦИИ
if(!sessionStorage.getItem('user')){
		Router('System','authForm');
	}
else{
		Router('ProjectPage','ProjectTable');
	}

// КЛИК ПО КНОПКЕ "ВОЙТИ"
	$("button.enterInSystem").live('click',
		function(){
			Router('System','authAction',[
				$("#login").val(),
				$("#passwd").val()
				]);
		});
		
////////////////////////////      END PAGE "АВТОРИЗАЦИЯ"  ////////////////////////////////////////////////

		

// КЛИК ПО "Проекты"
// Загружает страницу
	$("a[href='#project']").live('click',function(){
			Router('ProjectPage','ProjectTable');
	});
// КЛИК ПО "Договора"
// Загружает страницу
	$("a[href='#сontract']").live('click',function(){
			Router('ContractPage','ContractTable');
	});
// КЛИК ПО "Отказы"
// Загружает страницу
	$("a[href='#rejection']").live('click',function(){
			Router('RejectionPage','RejectionTable');
	});

	
// КЛИК ПО "Объект строительства" проекты (переход на виды работ)
// Загружает страницу
	$(".ProjectList_tr td:not(.tools)").live('click',function(){
			Router('TypeOfWorkPage','TypeOfWorkTable', $(this).closest('tr').attr('id'));
	});

// КЛИК ПО "Объект строительства" объекты договора (переход на виды работ)
// Загружает страницу
	$(".ObjectInContract_tr td:not(.tools)").live('click',function(){
			Router('TypeOfWorkPage','TypeOfWorkTable', $(this).closest('tr').attr('id'));
	});

// КЛИК ПО "Объект строительства" объекты отказа (переход на виды работ)
// Загружает страницу
	$(".ObjectInRejection_tr td:not(.tools)").live('click',function(){
			Router('TypeOfWorkPage','TypeOfWorkTable', $(this).closest('tr').attr('id'));
	});

///
	$("a[href='#typeofwork']").live('click',function(){
			Router('TypeOfWorkPage','TypeOfWorkTable', $('h6.object_name').attr('id'));
				
	});
	
	
//////////////////////////    START PAGE "ПРОЕКТЫ"		///////////////////////////////////////		
// КЛИК ПО "НОВЫЙ Проект"
	$("a#newproject").live('click', function(){
			var dw = $('#dialog_window');
				
				dw.load("php/router.php?c=ProjectPage&m=AddNewDialog");
				
				dw.dialog({
					title: "Создать новый объект",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		430,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#object_name").val()=="") {m=m+"    Наименование объекта"+"\n"};
						if ($("#object_contragent").val()=="") {m=m+"    Заказчик"+"\n"};
						if ($("#object_start_date").val()=="") {m=m+"    Начало работ"+"\n"};
						if ($("#object_end_date").val()=="") {m=m+"    Окончание работ"+"\n"};
						if ($("#object_personal").val()=="") {m=m+"    Ответственный от СХС"+"\n"};
						if ($("#object_limit_sum").val()=="") {m=m+"    Лимитная сумма"+"\n"};
						
						if (m=="") {
							Router('ProjectPage',"AddNewAction", [
												$("#object_name").val(),
												$("#object_contragent").val(),
												$("#object_start_date").val(),
												$("#object_end_date").val(),
												$("#object_personal").val(),
												$("#object_limit_sum").val(),
												$("#object_contract").val()
											]);
							if ($("#Cid").val()!=0){Router('ObjectInContract', 'ObjectInContractTable', $("#Cid").val())};				
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});

// КЛИК ПО "РЕДАКТРОВАТЬ"
// отображает поле с кнопками редактирования
	$("a#edit").live('click',function(){
			$(".editor").show();
		});
	
// КЛИК ПО ИКОНКЕ РЕДАКТРОВАНИЯ
	$('span.icon-edit').live('click',function(){
		var dw = $('#dialog_window'),
			Oid = $('h6.object_name').attr('id'),
			Wid = $('h6.work_name').attr('id');
			PP = $(this).closest('tr');

		// редактровать объект
		if(PP.hasClass("ProjectList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=ProjectPage&m=editDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать объект",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		430,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){

						//проверка заполнения обязательных полей
						var m="";
						if ($("#object_name").val()=="") {m=m+"    Наименование объекта"+"\n"};
						if ($("#object_contragent").val()=="") {m=m+"    Заказчик"+"\n"};
						if ($("#object_start_date").val()=="") {m=m+"    Начало работ"+"\n"};
						if ($("#object_end_date").val()=="") {m=m+"    Окончание работ"+"\n"};
						if ($("#object_personal").val()=="") {m=m+"    Ответственный от СХС"+"\n"};
						if ($("#object_limit_sum").val()=="") {m=m+"    Лимитная сумма"+"\n"};
						
						if (m=="") {

						Router('ProjectPage',"editAction", [
												$("#object_name").val(),
												$("#object_contragent").val(),
												$("#object_start_date").val(),
												$("#object_end_date").val(),
												$("#object_personal").val(),
												$("#object_limit_sum").val(),
												$("#object_contract").val(),
												Pid
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
		// редактровать работу
		if(PP.hasClass("WorkList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=TypeOfWorkPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать работу",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
							Router('TypeOfWorkPage',"editAction", [
												$("#work_name").val(),
												$("#work_builder").val(),
												$("#work_type").val(),
												Pid,
												Oid
											]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
		// редактровать материал план
		if(PP.hasClass("MaterialsPlanList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsPlanPage&m=editDialog&d="+Pid);
				// cutythbhjdfnm ahtqv d hjlbntkmcrjt jryj
				dw.dialog({
					title: "Редактировать план на материалы",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		400,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mp_name").val()=="") {m=m+"    Наименование материала"+"\n"};
						if ($("#mp_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mp_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mp_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mp_date").val()=="") {m=m+"    Дата"+"\n"};						
						if (m=="") {
						
							Router('MaterialsPlanPage',"editAction", [
												$("#mp_name").val(),
												$("#mp_measure").val(),
												$("#mp_amount").val(),
												$("#mp_price").val(),
												$("#mp_date").val(),
												$("#mp_disclaimer").val(),
												Pid,
												[Wid, Oid]
											]);
							$(this).dialog("close");
														}
						else {alert ("Не заполнены следующие поля:\n"+m);}	

						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

		// редактровать материал - заявка
		if(PP.hasClass("MaterialsBidList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsBidPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать заявку на материалы",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		550,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование материала"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата заявки"+"\n"};
						if (m=="") {

							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MaterialsBidPage","MaterialsBidTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}


		// редактровать материал - факт
		if(PP.hasClass("MaterialsFactList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsFactPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать запись (факт)",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		530,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование материала"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {

							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MaterialsFactPage","MaterialsFactTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

		// редактровать Машины/механизмы план
		if(PP.hasClass("MachineryPlanList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryPlanPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать план на машины/механизмы",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		400,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mp_name").val()=="") {m=m+"    Наименование машины/механизма"+"\n"};
						if ($("#mp_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mp_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mp_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mp_date").val()=="") {m=m+"    Дата"+"\n"};						
						if (m=="") {
						
							Router('MachineryPlanPage',"editAction", [
												$("#mp_name").val(),
												$("#mp_measure").val(),
												$("#mp_amount").val(),
												$("#mp_price").val(),
												$("#mp_date").val(),
												$("#mp_disclaimer").val(),
												Pid,
												[Wid, Oid]
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
							
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
			
		// редактровать Машины/механизмы - заявка
		if(PP.hasClass("MachineryBidList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryBidPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать заявку на машины/механизмы",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		550,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование машины/механизма"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата заявки"+"\n"};
						if (m=="") {

							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MachineryBidPage","MachineryBidTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

		// редактровать Машины/механизмы - факт
		if(PP.hasClass("MachineryFactList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryFactPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать запись (факт)",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		530,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование машины/механизма"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {

							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MachineryFactPage","MachineryFactTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
		// редактровать Трудозатраты план
		if(PP.hasClass("WorkPlanList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkPlanPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать план на трудозатраты",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		400,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mp_name").val()=="") {m=m+"    Наименование машины/механизма"+"\n"};
						if ($("#mp_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mp_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mp_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mp_date").val()=="") {m=m+"    Дата"+"\n"};						
						if (m=="") {
						
							Router('WorkPlanPage',"editAction", [
												$("#mp_name").val(),
												$("#mp_measure").val(),
												$("#mp_amount").val(),
												$("#mp_price").val(),
												$("#mp_date").val(),
												$("#mp_disclaimer").val(),
												Pid,
												[Wid, Oid]
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
							
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
			
		// редактровать Трудозатраты - заявка
		if(PP.hasClass("WorkBidList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkBidPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать заявку на трудозатраты",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		550,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Трудозатраты"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата заявки"+"\n"};
						if (m=="") {

							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("WorkBidPage","WorkBidTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
		// редактровать Трудозатраты - факт
		if(PP.hasClass("WorkFactList_tr")){
		var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkFactPage&m=editDialog&d="+Pid);
				dw.dialog({
					title: "Редактировать запись (факт)",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		530,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Трудозатраты"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {

							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("WorkFactPage","WorkFactTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

			
			
		//редактировать договор
		if(PP.hasClass("СontractList_tr")){
				var Pid = PP.attr('id');
			
				dw.load("php/router.php?c=ContractPage&m=editDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать договор",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		650,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						var selectedArray = new Array();
						var selObj =document.getElementById('contract_object');
						var i;
						var count = 0;
						  for (i=0; i<selObj.options.length; i++) {
							if (selObj.options[i].selected) {
							  selectedArray[count] = selObj.options[i].value;
							  count++;
							}
							};
						//проверка заполнения обязательных полей
						var m="";
						if ($("#contract_name").val()=="") {m=m+"    Имя договора"+"\n"};
						if ($("#contract_number").val()=="") {m=m+"    № договора"+"\n"};
						if ($("#contract_date").val()=="") {m=m+"    Дата"+"\n"};
						if ($("#contract_contragent").val()=="") {m=m+"    Заказчик"+"\n"};
						if ($("#contract_start_date").val()=="") {m=m+"    Начало работ"+"\n"};
						if ($("#contract_end_date").val()=="") {m=m+"    Окончание работ"+"\n"};
						if ($("#contract_sum_contract").val()=="") {m=m+"    Сумма по договору"+"\n"};
						
						if (m=="") {
						
							Router('ContractPage',"editAction", [
												$("#contract_name").val(),
												$("#contract_number").val(),
												$("#contract_date").val(),
												$("#contract_contragent").val(),
												$("#contract_start_date").val(),
												$("#contract_end_date").val(),
												$("#contract_sum_contract").val(),
												Pid,
												selectedArray
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

		// редактровать объект в контракте
		if(PP.hasClass("ObjectInContract_tr")){
		var Pid = PP.attr('id');
				SCid = $('tr.SelectedContract_tr').attr('id');
				dw.load("php/router.php?c=ObjectInContract&m=editDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать объект",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		430,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){

						//проверка заполнения обязательных полей
						var m="";
						if ($("#object_name").val()=="") {m=m+"    Наименование объекта"+"\n"};
						if ($("#object_contragent").val()=="") {m=m+"    Заказчик"+"\n"};
						if ($("#object_start_date").val()=="") {m=m+"    Начало работ"+"\n"};
						if ($("#object_end_date").val()=="") {m=m+"    Окончание работ"+"\n"};
						if ($("#object_personal").val()=="") {m=m+"    Ответственный от СХС"+"\n"};
						if ($("#object_limit_sum").val()=="") {m=m+"    Лимитная сумма"+"\n"};
						
						if (m=="") {

						Router('ObjectInContract',"editAction", [
												$("#object_name").val(),
												$("#object_contragent").val(),
												$("#object_start_date").val(),
												$("#object_end_date").val(),
												$("#object_personal").val(),
												$("#object_limit_sum").val(),
												$("#object_contract").val(),
												Pid,
												SCid
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

			
		//редактировать отказ
		if(PP.hasClass("RejectionList_tr")){
				var Pid = PP.attr('id');
			
				dw.load("php/router.php?c=RejectionPage&m=editDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать отказ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		450,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						
						var selectedArray = new Array();
						var selObj =document.getElementById('rejection_object');
						var i;
						var count = 0;
						  for (i=0; i<selObj.options.length; i++) {
							if (selObj.options[i].selected) {
							  selectedArray[count] = selObj.options[i].value;
							  count++;
							}
							};
						//проверка заполнения обязательных полей
						var m="";
						if ($("#rejection_name").val()=="") {m=m+"    Наименование"+"\n"};
						if ($("#rejection_number").val()=="") {m=m+"    № отказа"+"\n"};
						if ($("#rejection_date").val()=="") {m=m+"    Дата"+"\n"};
						
						if (m=="") {
						
							Router('RejectionPage',"editAction", [
												$("#rejection_name").val(),
												$("#rejection_number").val(),
												$("#rejection_date").val(),
												Pid,
												selectedArray
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
		// редактровать объект в отказе
		if(PP.hasClass("ObjectInRejection_tr")){
			SRid = $('tr.SelectedRejection_tr').attr('id');
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=ObjectInRejection&m=editDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать объект",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		430,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){

						//проверка заполнения обязательных полей
						var m="";
						if ($("#object_name").val()=="") {m=m+"    Наименование объекта"+"\n"};
						if ($("#object_contragent").val()=="") {m=m+"    Заказчик"+"\n"};
						if ($("#object_start_date").val()=="") {m=m+"    Начало работ"+"\n"};
						if ($("#object_end_date").val()=="") {m=m+"    Окончание работ"+"\n"};
						if ($("#object_personal").val()=="") {m=m+"    Ответственный от СХС"+"\n"};
						if ($("#object_limit_sum").val()=="") {m=m+"    Лимитная сумма"+"\n"};
						
						if (m=="") {

						Router('ObjectInRejection',"editAction", [
												$("#object_name").val(),
												$("#object_contragent").val(),
												$("#object_start_date").val(),
												$("#object_end_date").val(),
												$("#object_personal").val(),
												$("#object_limit_sum").val(),
												$("#object_contract").val(),
												Pid,
												SRid
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
			
		//редактировать справочники - контрагент
		if(PP.hasClass("HandbookContragent_tr")){
				var Pid = PP.attr('id');
			
				dw.load("php/router.php?c=HandbookPage&m=editContragentDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать запись - Контрагенты",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		450,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#contragent_name").val()=="") {m=m+"    Наименование"+"\n"};
						if ($("#contragent_adress").val()=="") {m=m+"    Адрес"+"\n"};
						if ($("#contragent_phone").val()=="") {m=m+"    Телефон"+"\n"};
						
						if (m=="") {
						
							Router('HandbookPage',"editContragentAction", [
												$("#contragent_name").val(),
												$("#contragent_adress").val(),
												$("#contragent_phone").val(),
												Pid
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

		//редактировать справочники - Субподрядчики
		if(PP.hasClass("HandbookBuilder_tr")){
				var Pid = PP.attr('id');
			
				dw.load("php/router.php?c=HandbookPage&m=editBuildertDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать запись - Субподрядчики",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		450,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#builder_name").val()=="") {m=m+"    Наименование"+"\n"};
						if ($("#builder_adress").val()=="") {m=m+"    Адрес"+"\n"};
						if ($("#rejection_date").val()=="") {m=m+"    Телефон"+"\n"};
						
						if (m=="") {
						
							Router('HandbookPage',"editBuilderAction", [
												$("#builder_name").val(),
												$("#builder_adress").val(),
												$("#builder_phone").val(),
												Pid
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}

		//редактировать справочники - сотрудники
		if(PP.hasClass("HandbookPersonal_tr")){
				var Pid = PP.attr('id');
			
				dw.load("php/router.php?c=HandbookPage&m=editPersonalDialog&d="+Pid);
				
				dw.dialog({
					title: "Редактировать запись - Сотрудники",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		450,
					resizable:	false,
					modal: true,
					buttons: {
						"Сохранить": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#personal_name").val()=="") {m=m+"    Ф.И.О."+"\n"};
						if (m=="") {
						
							Router('HandbookPage',"editPersonalAction", [
												$("#personal_name").val(),
												$("#personal_post").val(),
												$("#personal_department").val(),
												Pid
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
				$.getScript('js/kickstart.js');
			}
			
	});

// КЛИК ПО ИКОНКЕ УДАЛЕНИЯ
	$('span.icon-trash').live('click',function(){
				var dw = $('#dialog_window'),
					Oid = $('h6.object_name').attr('id'),
					Wid = $('h6.work_name').attr('id');
					PP = $(this).closest('tr');
					
			//удалить объект				
			if(PP.hasClass("ProjectList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=ProjectPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('ProjectPage',"deleteAction", Pid);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
			
			// удаляем работу
			if(PP.hasClass("WorkList_tr")){
			var Pid = PP.attr('id');
			
				dw.load("php/router.php?c=TypeOfWorkPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('TypeOfWorkPage',"deleteAction", [
																		Pid,
																		Oid
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
			
			// удаляем материал - план
			if(PP.hasClass("MaterialsPlanList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsPlanPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('MaterialsPlanPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}

			// удаляем  материал - заявка
			if(PP.hasClass("MaterialsBidList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsBidPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('MaterialsBidPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}

			// удаляем  материал - факт
			if(PP.hasClass("MaterialsFactList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsFactPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('MaterialsFactPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}

			// удаляем Машины/механизмы - план
			if(PP.hasClass("MachineryPlanList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryPlanPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('MachineryPlanPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
			
			
			// удаляем  Машины/механизмы - заявка
			if(PP.hasClass("MachineryBidList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryBidPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('MachineryBidPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}

			// удаляем  Машины/механизмы - факт
			if(PP.hasClass("MachineryFactList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryFactPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('MachineryFactPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}

			// удаляем Трудозатраты - план
			if(PP.hasClass("WorkPlanList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkPlanPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('WorkPlanPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
			
			
			
			// удаляем  Трудозатраты - заявка
			if(PP.hasClass("WorkBidList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkBidPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('WorkBidPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
			
			// удаляем  Трудозатраты - факт
			if(PP.hasClass("WorkFactList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkFactPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('WorkFactPage',"deleteAction", [
																		Pid,
																		[Wid, Oid]
																	]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
			
			
			//удалить договор
			if(PP.hasClass("СontractList_tr")){
			var Pid = PP.attr('id');
		
				dw.load("php/router.php?c=ContractPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		230,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
						
										dw.load("php/router.php?c=ContractPage&m=deleteobjectDialog");
														
										dw.dialog({
											autoOpen: 	false,
											show:		"blind",
											hide:		"blind",
											width:		500,
											height:		300,
											resizable:	false,
											modal: true,
											buttons: {
												"Удалить договор и объекты": function(){
													Router('ContractPage',"deleteobjectAction", Pid);
													$(this).dialog("close");
												},
												"Удалить договор, не удалять объекты": function(){
													Router('ContractPage',"deleteAction", Pid);
													$(this).dialog("close");
												},
												"Отмена": function(){$(this).dialog("close");}
											}
										});
										dw.dialog("open");

						
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");

	}

			//удалить объект в договоре				
			if(PP.hasClass("ObjectInContract_tr")){
			var Pid = PP.attr('id');
				SCid = $('tr.SelectedContract_tr').attr('id');
				dw.load("php/router.php?c=ObjectInContract&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('ObjectInContract',"deleteAction", [Pid, SCid]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
	
			//удалить отказ
			if(PP.hasClass("RejectionList_tr")){
			var Pid = PP.attr('id');
		
				dw.load("php/router.php?c=RejectionPage&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		230,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
						
										dw.load("php/router.php?c=RejectionPage&m=deleteobjectDialog");
														
										dw.dialog({
											autoOpen: 	false,
											show:		"blind",
											hide:		"blind",
											width:		500,
											height:		300,
											resizable:	false,
											modal: true,
											buttons: {
												"Удалить отказ и объекты": function(){
													Router('RejectionPage',"deleteobjectAction", Pid);
													$(this).dialog("close");
												},
												"Удалить отказ, не удалять объекты": function(){
													Router('RejectionPage',"deleteAction", Pid);
													$(this).dialog("close");
												},
												"Отмена": function(){$(this).dialog("close");}
											}
										});
										dw.dialog("open");

						
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");

	}

			//удалить объект в отказе				
			if(PP.hasClass("ObjectInRejection_tr")){
			var Pid = PP.attr('id');
				SRid = $('tr.SelectedRejection_tr').attr('id');
				dw.load("php/router.php?c=ObjectInRejection&m=deleteDialog&d="+Pid);
								
				dw.dialog({
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
							Router('ObjectInRejection',"deleteAction", [Pid, SRid]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
			}
	
			//удалить справочники - контрагент
			if(PP.hasClass("HandbookContragent_tr")){
			var Pid = PP.attr('id');
		
				dw.load("php/router.php?c=HandbookPage&m=deleteContragentDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		230,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
										dw.load("php/router.php?c=HandbookPage&m=deleteContragentAction&d="+Pid);
											dw.dialog({
											title:      "",
											autoOpen: 	false,
											show:		"blind",
											hide:		"blind",
											width:		500,
											height:		230,
											resizable:	false,
											modal: true,
											buttons: {
												"ОК": function(){
													Router('HandbookPage',"HandbookTable");
													$(this).dialog("close");
													}
											}
											});
										dw.dialog("open");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
	}

			//удалить справочники - Субподрядчики
			if(PP.hasClass("HandbookBuilder_tr")){
			var Pid = PP.attr('id');
		
				dw.load("php/router.php?c=HandbookPage&m=deleteBuilderDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		230,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
										dw.load("php/router.php?c=HandbookPage&m=deleteBuilderAction&d="+Pid);
											dw.dialog({
											title:      "",
											autoOpen: 	false,
											show:		"blind",
											hide:		"blind",
											width:		500,
											height:		230,
											resizable:	false,
											modal: true,
											buttons: {
												"ОК": function(){
													Router('HandbookPage',"HandbookTable");
													$(this).dialog("close");
													}
											}
											});
										dw.dialog("open");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
	}

			//удалить справочники - Сотрудники
			if(PP.hasClass("HandbookPersonal_tr")){
			var Pid = PP.attr('id');
		
				dw.load("php/router.php?c=HandbookPage&m=deletePersonalDialog&d="+Pid);
								
				dw.dialog({
					title:      "",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		500,
					height:		230,
					resizable:	false,
					modal: true,
					buttons: {
						"Удалить": function(){
										dw.load("php/router.php?c=HandbookPage&m=deletePersonalAction&d="+Pid);
											dw.dialog({
											title:      "",
											autoOpen: 	false,
											show:		"blind",
											hide:		"blind",
											width:		500,
											height:		230,
											resizable:	false,
											modal: true,
											buttons: {
												"ОК": function(){
													Router('HandbookPage',"HandbookTable");
													$(this).dialog("close");
													}
											}
											});
										dw.dialog("open");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				dw.dialog("open");
	}
	
	
	});

// КЛИК ПО "НОВЫЙ договор"
	$("a#newcontract").live('click', function(){
			var dw = $('#dialog_window');
				
				dw.load("php/router.php?c=ContractPage&m=AddNewDialog");
				
				dw.dialog({
					title: "Создать новый договор",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		650,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						
						var selectedArray = new Array();
						var selObj =document.getElementById('contract_object');
						var i;
						var count = 0;
						  for (i=0; i<selObj.options.length; i++) {
							if (selObj.options[i].selected) {
							  selectedArray[count] = selObj.options[i].value;
							  count++;
							}
							};
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#contract_name").val()=="") {m=m+"    Имя договора"+"\n"};
						if ($("#contract_number").val()=="") {m=m+"    № договора"+"\n"};
						if ($("#contract_date").val()=="") {m=m+"    Дата"+"\n"};
						if ($("#contract_contragent").val()=="") {m=m+"    Заказчик"+"\n"};
						if ($("#contract_start_date").val()=="") {m=m+"    Начало работ"+"\n"};
						if ($("#contract_end_date").val()=="") {m=m+"    Окончание работ"+"\n"};
						if ($("#contract_sum_contract").val()=="") {m=m+"    Сумма по договору"+"\n"};
						
						if (m=="") {
							Router('contractPage',"AddNewAction", [
												$("#contract_name").val(),
												$("#contract_number").val(),
												$("#contract_date").val(),
												$("#contract_contragent").val(),
												$("#contract_start_date").val(),
												$("#contract_end_date").val(),
												$("#contract_sum_contract").val(),
												selectedArray
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});

// КЛИК ПО "РЕДАКТРОВАТЬ" договор
// отображает поле с кнопками редактирования
	$("a#editcontract").live('click',function(){
			$(".editor").show();
		});
	


//клик по строке в таблице договоров
$('.СontractList_tr td:not(.tools)').live('click', function(){	
	Pid = $(this).closest('tr').attr('id');         
	Router('ObjectInContract','ObjectInContractTable', Pid);
});


// КЛИК ПО "НОВЫЙ отказ"
	$("a#newrejection").live('click', function(){
			var dw = $('#dialog_window');
				
				dw.load("php/router.php?c=RejectionPage&m=AddNewDialog");
				
				dw.dialog({
					title: "Создать новый отказ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		450,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						
						var selectedArray = new Array();
						var selObj =document.getElementById('rejection_object');
						var i;
						var count = 0;
						  for (i=0; i<selObj.options.length; i++) {
							if (selObj.options[i].selected) {
							  selectedArray[count] = selObj.options[i].value;
							  count++;
							}
							};
						
						//проверка заполнения обязательных полей
						var m="";
						if ($("#rejection_name").val()=="") {m=m+"    Наименование"+"\n"};
						if ($("#rejection_number").val()=="") {m=m+"    № отказа"+"\n"};
						if ($("#rejection_date").val()=="") {m=m+"    Дата"+"\n"};
						
						if (m=="") {
							Router('RejectionPage',"AddNewAction", [
												$("#rejection_name").val(),
												$("#rejection_number").val(),
												$("#rejection_date").val(),
												selectedArray
											]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});

// КЛИК ПО "РЕДАКТРОВАТЬ" отказ
// отображает поле с кнопками редактирования
	$("a#editrejection").live('click',function(){
			$(".editor").show();
		});
	


//клик по строке в таблице отказов
$('.RejectionList_tr td:not(.tools)').live('click', function(){	
	Pid = $(this).closest('tr').attr('id');         
	Router('ObjectInRejection','ObjectInRejectionTable', Pid);
});
	
	
	
//////////////////////////    END PAGE "ПРОЕКТЫ"		///////////////////////////////////////

//////////////////////////	START PAGE WORK \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

// КЛИК ПО "Новая работа"
	$("a#newwork").live('click', function(){
			var dw = $('#dialog_window'),
				Pid = $('h6.object_name').attr('id');	
			
				dw.load("php/router.php?c=TypeOfWorkPage&m=AddNewDialog");
				
				dw.dialog({
					title: "Создать новую работу",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		300,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//необходимо добавить проверку заполнения обязательных полей
							Router('TypeOfWorkPage',"AddNewAction", [
												$("#work_name").val(),
												$("#work_builder").val(),
												$("#work_type").val(),
												Pid
											]);
							$(this).dialog("close");
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	
	// КЛИК ПО "Новый материал" план
	$("a#newmaterialplan").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=MaterialsPlanPage&m=AddNewDialog");
				
				dw.dialog({
					title: "Создать план",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		400,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mp_name").val()=="") {m=m+"    Наименование материала"+"\n"};
						if ($("#mp_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mp_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mp_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mp_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {
							Router('MaterialsPlanPage',
									"AddNewAction", 
									[
										$("#mp_name").val(),
										$("#mp_measure").val(),
										$("#mp_amount").val(),
										$("#mp_price").val(),
										$("#mp_date").val(),
										$("#mp_disclaimer").val(),
										[Wid, Oid]
									]);
							$(this).dialog("close");
																					}
						else {alert ("Не заполнены следующие поля:\n"+m);}	

						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});

	// КЛИК ПО "Новая заявка" (материалы)
	$("a#newmaterialbid").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=MaterialsBidPage&m=AddNewDialog&d="+Wid);
				
				dw.dialog({
					title: "Создать заявку",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		500,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование материала"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата заявки"+"\n"};
						if (m=="") {
							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MaterialsBidPage","MaterialsBidTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});

	
	// КЛИК ПО "Новая запись" (материал - факт)
	$("a#newmaterialfact").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=MaterialsFactPage&m=AddNewDialog&d="+Wid);
				
				dw.dialog({
					title: "Создать новую запись (факт)",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		520,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование материала"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {
							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MaterialsFactPage","MaterialsFactTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	
	
	// КЛИК ПО "Новая запись" (машины/механизмы - план)
	$("a#newmachineryplan").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=MachineryPlanPage&m=AddNewDialog");
				
				dw.dialog({
					title: "Создать план",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		400,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mp_name").val()=="") {m=m+"    Наименование машины/механизма"+"\n"};
						if ($("#mp_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mp_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mp_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mp_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {
							Router('MachineryPlanPage',
									"AddNewAction", 
									[
										$("#mp_name").val(),
										$("#mp_measure").val(),
										$("#mp_amount").val(),
										$("#mp_price").val(),
										$("#mp_date").val(),
										$("#mp_disclaimer").val(),
										[Wid, Oid]
									]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
							
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	
	
	// КЛИК ПО "Новая заявка" (машины/механизмы)
	$("a#newmachinerybid").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=MachineryBidPage&m=AddNewDialog&d="+Wid);
				
				dw.dialog({
					title: "Создать заявку",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		500,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование машины/механизма"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата заявки"+"\n"};
						if (m=="") {
							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MachineryBidPage","MachineryBidTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	
	// КЛИК ПО "Новая запись" (машины/механизмы - факт)
	$("a#newmachineryfact").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=MachineryFactPage&m=AddNewDialog&d="+Wid);
				
				dw.dialog({
					title: "Создать новую запись (факт)",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		520,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Наименование машины/механизма"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {
							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("MachineryFactPage","MachineryFactTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	

	// КЛИК ПО "Новая запись" (Трудозатраты - план)
	$("a#newworkplan").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=WorkPlanPage&m=AddNewDialog");
				
				dw.dialog({
					title: "Создать план",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		400,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mp_name").val()=="") {m=m+"    Трудозатраты"+"\n"};
						if ($("#mp_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mp_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mp_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mp_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {
							Router('WorkPlanPage',
									"AddNewAction", 
									[
										$("#mp_name").val(),
										$("#mp_measure").val(),
										$("#mp_amount").val(),
										$("#mp_price").val(),
										$("#mp_date").val(),
										$("#mp_disclaimer").val(),
										[Wid, Oid]
									]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
							
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	
	
	
	// КЛИК ПО "Новая заявка" (Трудозатраты)
	$("a#newworkbid").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=WorkBidPage&m=AddNewDialog&d="+Wid);
				
				dw.dialog({
					title: "Создать заявку",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		500,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Трудозатраты"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата заявки"+"\n"};
						if (m=="") {
							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("WorkBidPage","WorkBidTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	
	
	// КЛИК ПО "Новая запись" (Трудозатраты - факт)
	$("a#newworkfact").live('click', function(){
			var dw = $('#dialog_window'),
				Oid = $('h6.object_name').attr('id'),
				Wid = $('h6.work_name').attr('id');
				dw.load("php/router.php?c=WorkFactPage&m=AddNewDialog&d="+Wid);
				
				dw.dialog({
					title: "Создать новую запись (факт)",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		520,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#mb_name").val()=="") {m=m+"    Трудозатраты"+"\n"};
						if ($("#mb_measure").val()=="") {m=m+"    Ед. измерения"+"\n"};
						if ($("#mb_amount").val()=="") {m=m+"    Количество"+"\n"};
						if ($("#mb_price").val()=="") {m=m+"    Цена за ед."+"\n"};
						if ($("#mb_date").val()=="") {m=m+"    Дата"+"\n"};
						if (m=="") {
							$(this).dialog("close");
							$("#mb_doc").submit();
							setTimeout(function(){Router("WorkFactPage","WorkFactTable", [Wid, Oid])},3000);
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
	});
	
	
	
//*************************************работа с картинками*******************************	
// КЛИК ПО ИКОНКЕ документ 
	$('span.icon-file-alt').live('click',function(){
				var dw = $('#dialog_window'),
					Oid = $('h6.object_name').attr('id'),
					Wid = $('h6.work_name').attr('id');
					PP = $(this).closest('tr');
					
			//показать документ (материалы - заявка)				
			if(PP.hasClass("MaterialsBidList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsBidPage&m=showDocDialog&d="+Pid);
								
				dw.dialog({
					title: "Документ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		1000,
					height:		900,
					resizable:	false,
					modal: true,
					buttons: {
						"Печать": function(){
						var item=document.getElementById('doc_image').src;
						var pw = window.open(item);
						pw.print();
						},
						"Отмена": function(){$(this).dialog("close");
											 	}												
												
					},
					close: function(){Router('system','clearTMP');
					                  Router('MaterialsBidPage','MaterialsBidTable', [Wid, Oid])}
				});
				dw.dialog("open");

				  }

			//показать документ (материалы - факт)				
			if(PP.hasClass("MaterialsFactList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MaterialsFactPage&m=showDocDialog&d="+Pid);
								
				dw.dialog({
					title: "Документ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		1000,
					height:		900,
					resizable:	false,
					modal: true,
					buttons: {
				/* 		"Печать": function(){
						var item=document.getElementById('doc_image').src;
						var pw = window.open(item);
						pw.print();
						},*/
						"Отмена": function(){$(this).dialog("close");
											 	}
					}, 
					close: function(){Router('system','clearTMP');
					                  Router('MaterialsFactPage','MaterialsFactTable', [Wid, Oid])}
				});
				dw.dialog("open");

				  }

		  //показать документ (машины/механизмы - заявка)				
			if(PP.hasClass("MachineryBidList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryBidPage&m=showDocDialog&d="+Pid);
								
				dw.dialog({
					title: "Документ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		1000,
					height:		900,
					resizable:	false,
					modal: true,
					buttons: {
						"Печать": function(){
						var item=document.getElementById('doc_image').src;
						var pw = window.open(item);
						pw.print();
						},
						"Отмена": function(){$(this).dialog("close");
											 	}
					},
					close: function(){Router('system','clearTMP');
					                  Router('MachineryBidPage','MachineryBidTable', [Wid, Oid])}
				});
				dw.dialog("open");

				  }
			//показать документ (машины/механизмы - факт)				
			if(PP.hasClass("MachineryFactList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=MachineryFactPage&m=showDocDialog&d="+Pid);
								
				dw.dialog({
					title: "Документ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		1000,
					height:		900,
					resizable:	false,
					modal: true,
					buttons: {
						"Печать": function(){
						var item=document.getElementById('doc_image').src;
						var pw = window.open(item);
						pw.print();
						},
						"Отмена": function(){$(this).dialog("close");
											 	}
					},
					close: function(){Router('system','clearTMP');
					                  Router('MachineryFactPage','MachineryFactTable', [Wid, Oid])}
				});
				dw.dialog("open");

				  }

			//показать документ (Трудозатраты - заявка)				
			if(PP.hasClass("WorkBidList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkBidPage&m=showDocDialog&d="+Pid);
								
				dw.dialog({
					title: "Документ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		1000,
					height:		900,
					resizable:	false,
					modal: true,
					buttons: {
						"Печать": function(){
						var item=document.getElementById('doc_image').src;
						var pw = window.open(item);
						pw.print();
						},
						"Отмена": function(){$(this).dialog("close");
											 	}
					},
					close: function(){Router('system','clearTMP');
					                  Router('WorkBidPage','WorkBidTable', [Wid, Oid])}
				});
				dw.dialog("open");

				  }

	
			//показать документ (Трудозатраты - факт)				
			if(PP.hasClass("WorkFactList_tr")){
			var Pid = PP.attr('id');
				dw.load("php/router.php?c=WorkFactPage&m=showDocDialog&d="+Pid);
								
				dw.dialog({
					title: "Документ",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		1000,
					height:		900,
					resizable:	false,
					modal: true,
					buttons: {
						"Печать": function(){
						var item=document.getElementById('doc_image').src;
						var pw = window.open(item);
						pw.print();
						},
						"Отмена": function(){$(this).dialog("close");
											 	}
					},
					close: function(){Router('system','clearTMP');
					                  Router('WorkFactPage','WorkFactTable', [Wid, Oid])}
				});
				dw.dialog("open");

				  }
	
				  
	});
					

//печать (факт)
function printImage(filename){
						//var item=document.getElementById('doc_image').src;
						var pw = window.open(filename);
						pw.print();
};

					
//////////////////////////	END PAGE WORK \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
///////////////////////////////////////////////////////////////////////////////////
// ВЫПАДАЮЩЕЕ МЕНЮ	разделов
$.contextMenu({
        selector: '.WorkList_tr td:not(.tools)',
		trigger: 'left',
        callback: function(key, opt) {
				$tabs = $('#tabs').tabs();
				Tid = $tabs.tabs('option', 'selected');

			Router('MMWPage','route',[
						$(this).parent().attr('id'),// id работы
						key,	// [раздел]-[таблица] (MMV-PBD)
						$('h6.object_name').attr('id'), // id объекта,
						Tid //номер выбранной вкладки
					]);
        },
        items: {
            "materials": {"name": "Материалы",
				"items": {
					"materials-plan": {"name": "План"},
					"materials-bid":  {"name": "Заявка"},
					"materials-fact": {"name": "Факт"},
				}},
            "machinery": {"name": "Машины\\Механизмы",
				"items": {
					"machinery-plan": {"name": "План"},
					"machinery-bid":  {"name": "Заявка"},
					"machinery-fact": {"name": "Факт"},
				}},
            "work": {"name": "Трудозатраты",
				"items": {
					"work-plan": {"name": "План"},
					"work-bid":  {"name": "Заявка"},
					"work-fact": {"name": "Факт"},
				}},
        }
    });

/////////////////////////////////////////////////////////////////////////////////	
// КЛИК ПО "Справочники"
// Загружает страницу
	$("a[href='#handbook']").live('click',function(){
			Router('HandbookPage','HandbookTable');
	});

// КЛИК ПО "Отчеты"
// Загружает страницу
	$("a[href='#report']").live('click',function(){
			Router('ReportPage','ReportTable');
	});
	
	// КЛИК ПО "Новая запись" 
	$("a#newhandbook").live('click', function(){
			var dw = $('#dialog_window'),
				$tabs = $('#tabs').tabs();
				Tid = $tabs.tabs('option', 'selected');
				
				//Контрагенты
				if (Tid==0){
				
				dw.load("php/router.php?c=HandbookPage&m=AddNewContragentDialog");

				dw.dialog({
					title: "Новая запись - Контрагенты",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		370,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#contragent_name").val()=="") {m=m+"    Наименование"+"\n"};
						if ($("#contragent_adress").val()=="") {m=m+"    Адрес"+"\n"};
						if ($("#contragent_phone").val()=="") {m=m+"    Телефон"+"\n"};
						if (m=="") {
							Router('HandbookPage',"AddNewContragentAction", 
									[
										$("#contragent_name").val(),
										$("#contragent_adress").val(),
										$("#contragent_phone").val()
									]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
							
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
		}		
		
				//Субподрядчики
				if (Tid==1){
				
				dw.load("php/router.php?c=HandbookPage&m=AddNewBuilderDialog");

				dw.dialog({
					title: "Новая запись - Субподрядчики",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		370,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#builder_name").val()=="") {m=m+"    Наименование"+"\n"};
						if ($("#builder_adress").val()=="") {m=m+"    Адрес"+"\n"};
						if ($("#builder_phone").val()=="") {m=m+"    Телефон"+"\n"};
						if (m=="") {
							Router('HandbookPage',"AddNewBuilderAction", 
									[
										$("#builder_name").val(),
										$("#builder_adress").val(),
										$("#builder_phone").val()
									]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
							
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
		}		

				//Сотрудники
				if (Tid==2){
				
				dw.load("php/router.php?c=HandbookPage&m=AddNewPersonalDialog");

				dw.dialog({
					title: "Новая запись - Сотрудники",
					autoOpen: 	false,
					show:		"blind",
					hide:		"blind",
					width:		430,
					height:		370,
					resizable:	false,
					modal: true,
					buttons: {
						"Создать": function(){
						//проверка заполнения обязательных полей
						var m="";
						if ($("#personal_name").val()=="") {m=m+"    Ф.И.О."+"\n"};
						if (m=="") {
							Router('HandbookPage',"AddNewPersonalAction", 
									[
										$("#personal_name").val(),
										$("#personal_post").val(),
										$("#personal_department").val()
									]);
							$(this).dialog("close");
							}
						else {alert ("Не заполнены следующие поля:\n"+m);}	
							
						},
						"Отмена": function(){$(this).dialog("close");}
					}
				});
				
				dw.dialog("open");
				$.getScript('js/kickstart.js');
		}		
		
	});
	
//ОТЧЕТЫ
// report
	$("button.report").live('click',
		function(){
			var report_type =document.getElementById('report_type');
			Router('system','clearReport'); //чистим папку report
			//отчет1
			if (report_type.selectedIndex==1) {
				Router('ReportPage','Report1', $("#object").val());
				//var f=Router('ReportPage','Report1', $("#object").val());
				Router('ReportPage','ReportTable');
			};
			
			//отчет2
			if (report_type.selectedIndex==2) {
				var f=Router('ReportPage','Report2');
				Router('ReportPage','ReportTable');
			};

			//отчет3
			if (report_type.selectedIndex==3) {
				var f=Router('ReportPage','Report3', [$("#object").val(), 
													  $("#report_year").val()]);
				Router('ReportPage','ReportTable');
			};
			
		});
	

});

