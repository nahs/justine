<?php
/****************************/
//	@system 		Justine
//	@description 	контроллер страницы "Договоры"
//	@autor 			ilsiya
//  @email 			
//	@create			2013
//	@version		4.13
/****************************/
class ContractPage extends System{
//	Таблица "Договора"
//	@return	таблицу договоров
	public static function ContractTable(){
		$dbh = parent::dbConnect();
		$res = $dbh->query("SELECT contract.id as id, contract.name as name, number, date, contragent.name as contragent, start_date, end_date, sum_contract FROM contract left join contragent on contragent.id=contract.id_contragent")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
		//вычисляем плановую и фактическую сумму
		$machineryfact_sum = $dbh->query("select sum(amount*price) from machineryfact
										where worktype in (select id from typeofwork
										where typeofwork.id_object in 
										(select id_object from object_in_contract where id_contract=".$v->id."))")->fetchAll(PDO::FETCH_COLUMN);
		$machineryplan_sum = $dbh->query("select sum(amount*price) from machineryplan
										where worktype in (select id from typeofwork
										where typeofwork.id_object in 
										(select id_object from object_in_contract where id_contract=".$v->id."))")->fetchAll(PDO::FETCH_COLUMN);
		$materialsfact_sum = $dbh->query("select sum(amount*price) from materialsfact
										where worktype in (select id from typeofwork
										where typeofwork.id_object in 
										(select id_object from object_in_contract where id_contract=".$v->id."))")->fetchAll(PDO::FETCH_COLUMN);
		$materialsplan_sum = $dbh->query("select sum(amount*price) from materialsplan
										where worktype in (select id from typeofwork
										where typeofwork.id_object in 
										(select id_object from object_in_contract where id_contract=".$v->id."))")->fetchAll(PDO::FETCH_COLUMN);
		$workfact_sum = $dbh->query("select sum(amount*price) from workfact
										where worktype in (select id from typeofwork
										where typeofwork.id_object in 
										(select id_object from object_in_contract where id_contract=".$v->id."))")->fetchAll(PDO::FETCH_COLUMN);
		$workplan_sum = $dbh->query("select sum(amount*price) from workplan
										where worktype in (select id from typeofwork
										where typeofwork.id_object in 
										(select id_object from object_in_contract where id_contract=".$v->id."))")->fetchAll(PDO::FETCH_COLUMN);
							
			$t .= '<tr id="'.$v->id.'" class="line СontractList_tr">
					<td class="left" width="auto">'.$v->name.'</td>
					<td class="center" width="200px">'.$v->number.'</td>
					<td class="center" width="100px">'.$v->date.'</td>
					<td class="center" width="100px">'.$v->contragent.'</td>
					<td class="center" width="100px">'.$v->start_date.'</td>
					<td class="center" width="100px">'.$v->end_date.'</td>
					<td class="center" width="100px">'.$v->sum_contract.'</td>
					<td class="center" width="100px">'.($machineryplan_sum[0]+$materialsplan_sum[0]+$workplan_sum[0]).'</td>
					<td class="center" width="100px">'.($machineryfact_sum[0]+$materialsfact_sum[0]+$workfact_sum[0]).'</td>
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>';
		}
		
		$a = array(
			'html' 	=> '
						<nav id="top-nav">
							<ul class="menu left">
								<li><a href="#project">Проекты</a></li>
								<li><a href="#сontract">Договоры</a></li>
								<li><a href="#rejection">Отказы</a></li>
								<li><a href="#handbook">Справочники</a></li>
								<li><a href="#report">Отчеты</a></li>
								<li class="adm"><a style="padding-right: 20px; font-size: 20px;"><span class="icon-cog"></span></a>
									<ul>
										<li><a id="newcontract" style="padding-right: 10px;"><span class="icon-file"></span>Новый договор</a></li>
										<li><a id="editcontract" style="padding-right: 10px;"><span class="icon-edit"></span>Редактировать</a></li>
									</ul>							
								</li>
							</ul>
						</nav>
						<section>							
						<table id="ContractList" cellspacing="0" cellpadding="0" class="sortable">
							<thead><tr>
								<th class="center">Имя договора</th>
								<th class="center">№ договора</th>
								<th class="center">Дата</th>
								<th class="center">Заказчик</th>
								<th class="center">Начало работ</th>
								<th class="center">Окончание работ</th>
								<th class="center">Сумма по договору</th>
								<th class="center">Плановая сумма</th>
								<th class="center">Фактическая сумма</th>
								<th class="editor">Редактор</th>
							</tr></thead>
							<tbody>
								'.$t.'
							</tbody>
							</table>
                        </section>'
			);
			
		return json_encode($a);
	}
// форма для добавления нового договора
	public static function AddNewDialog(){
		$dbh = parent::dbConnect();
		$res = $dbh->query("SELECT id, name FROM contragent")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
			$contragent .= '<option value="'.$v->id.'">'.$v->name.'</option>';
		}
		$res = $dbh->query("SELECT id, name FROM object where connection='project'")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
			$object .= '<option value="'.$v->id.'">'.$v->name.'</option>';
		}

  	    return '
			<p>
				<input type="text" placeholder="Имя договора" id="contract_name" class="col_12">
			</p>
			<p>
				<input type="text" placeholder="№ договора" id="contract_number" class="col_12">
			</p>
			<p>
				<input type="text" class="calendar col_6" placeholder="Дата" id="contract_date">
			</p>
			<p>
				<select id="contract_contragent" data-placeholder="Заказчик" class="col_12">
					<option value="">Заказчик</option>
					'.$contragent.'
				</select>
			</p>
			<p>
				<input type="text" class="calendar_from col_6" placeholder="Начало работ" id="contract_start_date">
				<input type="text" class="calendar_to col_6" placeholder="Окончание работ" id="contract_end_date">
			</p>
			<p>
				<input type="text" placeholder="Сумма по договору" id="contract_sum_contract" class="col_12">
			</p>
			<p>
				<select multiple id="contract_object" data-placeholder="Проекты" class="col_12" size="7">
					<option value="">Проекты</option>
					'.$object.'
				</select>
			</p>

			';
	}
	
//	Запись нового объекта в базу данных
/* @input $data - Array(
						[0] => Имя договора
						[1] => № договора
						[2] => дата
						[3] => идентификатор заказчика
						[4] => дата начала дд.мм.гггг
						[5] => дата окончания дд.мм.гггг
						[6] => Сумма по договору
						[7] => идентификаторы объектов
					)
*/
// @return таблица "договоры"
	public static function AddNewAction($data){
		$dbh = parent::dbConnect();
		$dbh->exec("INSERT INTO contract VALUES ('', '".$data[0]."', '".$data[1]."', '".parent::ConvertDate($data[2], "toMySql")."', '".$data[3]."', '".parent::ConvertDate($data[4], "toMySql")."', '".parent::ConvertDate($data[5], "toMySql")."', '".$data[6]."')");
		$lastInsertId = $dbh->lastInsertId();
		
			//добавляем данные в таблицу object_in_contract и редактируем таблицу object
			if($data[7] != null){
			foreach($data[7] as $v){
			$dbh->exec("INSERT INTO object_in_contract VALUES ('','".$v."','".$lastInsertId."')");
			$dbh->exec("update object set connection='contract' where id='".$v."'");
			}
			}	
		return self::ContractTable();
				
	}

// Окно подтверждения удаления объекта из таблицы "договоры"
// @input идентификатор объекта
	public static function deleteDialog($data){
		$r = parent::myquery("SELECT name FROM contract WHERE id=".$data, PDO::FETCH_COLUMN);
		return "<div class='center' style='font-size: 20px;'>Удалить договор<br><br><strong>".$r[0]."</strong><br><br><span style='font-size: 30px'>?</span></div>";
	}

// Окно подтверждения удаления объектов из таблицы "объекты"
// @input идентификатор объекта
	public static function deleteobjectDialog(){
		return "<div class='center' style='font-size: 20px;'>Удалить объекты, <br><br>прикрепленные к данному договору<br><br><span style='font-size: 30px'>?</span></div>";
	}

	
// Удаление объекта из таблицы "договоры" (только договор)
// @input идентификатор объекта
	public static function deleteAction($data){
		$dbh = parent::dbConnect();
		$dbh->exec("update object set connection='project' where id in (select id_object FROM object_in_contract WHERE id_contract=".$data.")");
		$dbh->exec("DELETE FROM object_in_contract WHERE id_contract = '".$data."'"); 
		$dbh->exec("DELETE FROM contract WHERE contract.id = '".$data."'"); 
		return self::ContractTable();
	}

// Удаление объекта из таблицы "договоры" (и договор и объекты)
// @input идентификатор объекта
	public static function deleteobjectAction($data){
		$dbh = parent::dbConnect();
		$dbh->exec("delete from object where id in (select id_object FROM object_in_contract WHERE id_contract=".$data.")");
		$dbh->exec("DELETE FROM object_in_contract WHERE id_contract = '".$data."'"); 
		$dbh->exec("DELETE FROM contract WHERE contract.id = '".$data."'"); 
		return self::ContractTable();
	}

	
// Окно редактирования объекта из таблицы "договоры"
// @input идентификатор объекта
	public static function editDialog($data){
		$dbh = parent::dbConnect();
		//список заказчиков
		$r = $dbh->query("SELECT id_contragent FROM contract WHERE id=".$data)->fetchAll(PDO::FETCH_COLUMN);
		$res = $dbh->query("SELECT id, name FROM contragent")->fetchAll(PDO::FETCH_OBJ);
			foreach($res as $v){
				if($r[0] == $v->id)
					$contragent .= '<option value="'.$v->id.'" selected>'.$v->name.'</option>';
				else
					$contragent .= '<option value="'.$v->id.'">'.$v->name.'</option>';
			}

		$contract = $dbh->query("SELECT * FROM contract WHERE id=".$data)->fetchAll(PDO::FETCH_OBJ);

		//список объектов (выделены те, которые прикреплены к данному договору)
		//сначала выделенные - потом остальные
		$res = $dbh->query("SELECT id, name FROM object where id in (select id_object FROM object_in_contract WHERE id_contract=".$data.")")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
			$object .= '<option value="'.$v->id.'" selected>'.$v->name.'</option>';
		}
		$res = $dbh->query("SELECT id, name FROM object where connection='project'")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
			$object .= '<option value="'.$v->id.'" >'.$v->name.'</option>';
		}
		
		
		return '
				<p>
					<input type="text" placeholder="Имя договора" id="contract_name" class="col_12" value="'.htmlspecialchars($contract[0]->name).'">
				</p>
				<p>
					<input type="text" placeholder="№ договора" id="contract_number" class="col_12" value="'.htmlspecialchars($contract[0]->number).'">
				<p>
					<input type="text" class="calendar_to col_6" placeholder="Дата" id="contract_date" value="'.parent::ConvertDate($contract[0]->date, "fromMySql").'">
				</p>
				<p>
					<select id="contract_contragent" data-placeholder="Заказчик" class="col_12">
						<option value="">Заказчик</option>
						'.$contragent.'
					</select>
				</p>
				<p>
					<input type="text" class="calendar_from col_6" placeholder="Начало работ" id="contract_start_date" value="'.parent::ConvertDate($contract[0]->start_date, "fromMySql").'">
					<input type="text" class="calendar_to col_6" placeholder="Окончание работ" id="contract_end_date" value="'.parent::ConvertDate($contract[0]->end_date, "fromMySql").'">
				</p>
				<p>
					<input type="text" placeholder="Сумма по договору" id="contract_sum_contract" class="col_12" value="'.htmlspecialchars($contract[0]->sum_contract).'">
				</p>
				<p>
					<select multiple id="contract_object" data-placeholder="Проекты" class="col_12" size="7">
						<option value="">Проекты</option>
						'.$object.'
					</select>
				</p>

				';	
	}
	
//	Обновление объекта в базе данных
/* @input $data - Array(
						[0] => Имя договора
						[1] => № договора
						[2] => дата
						[3] => идентификатор заказчика
						[4] => дата начала дд.мм.гггг
						[5] => дата окончания дд.мм.гггг
						[6] => Сумма по договору
						[7] => идентификатор объекта
						[8] => идентификаторы object_in_contract
						
					)
*/
// @return таблица "договоры"
	public static function editAction($data){
		$dbh = parent::dbConnect();
		$dbh->exec("UPDATE contract SET name='".$data[0]."', number='".$data[1]."', date='".parent::ConvertDate($data[2], "toMySql")."', id_contragent=".$data[3].", start_date='".parent::ConvertDate($data[4], "toMySql")."', end_date='".parent::ConvertDate($data[5], "toMySql")."', sum_contract='".$data[6]."'  WHERE id=".$data[7]);
		
			//редактируем прикрепленные к договору объекты	
			if($data[8] != null){
			$dbh->exec("update object set connection='project' where id in (select id_object FROM object_in_contract WHERE id_contract=".$data[7].")");
			$dbh->exec("delete from object_in_contract where id_contract='".$data[7]."'");
				foreach($data[8] as $v){
				$dbh->exec("INSERT INTO object_in_contract VALUES ('','".$v."','".$data[7]."')");
				$dbh->exec("update object set connection='contract' where id='".$v."'");
				}
			}
		
		return self::ContractTable();
	}
	
}
?>