<?php
/****************************/
//	@system 		Justine
//	@description 	контроллер страницы "отказы"
//	@autor 			ilsiya
//  @email 			
//	@create			2013
//	@version		4.13
/****************************/

class ObjectInRejection extends System{

	public static function ObjectInRejectionTable($data){
	
    	$dbh = parent::dbConnect();
		$res = $dbh->query("SELECT * FROM rejection where id='".$data."'")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
			$rejection .= '<tr id="'.$v->id.'" class="line SelectedRejection_tr">
					<td class="left" width="auto">'.$v->name.'</td>
					<td class="center" width="100px">'.$v->number.'</td>
					<td class="center" width="100px">'.$v->date.'</td>
				</tr>';
		}

		$res = $dbh->query("SELECT id, name, start_date, end_date, limit_sum FROM object where id in (select id_object from object_in_rejection where id_rejection='".$data."')")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
			
			$t .= '<tr id="'.$v->id.'" class="line ObjectInRejection_tr">
					<td class="left" width="auto">'.$v->name.'</td>
					<td class="center" width="100px">'.$v->start_date.'</td>
					<td class="center" width="100px">'.$v->end_date.'</td>
					<td class="center" width="100px">'.$v->limit_sum.'</td>
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>';
		}
		
		
		$a = array(
			'html' 	=> '
						<nav id="top-nav">
							<ul class="menu left">
								<li><a href="#project">Проекты</a></li>
								<li><a href="#сontract">Договоры</a></li>
								<li><a href="#rejection">Отказы</a></li>
								<li><a href="#handbook">Справочники</a></li>
								<li><a href="#report">Отчеты</a></li>
								<li class="adm"><a style="padding-right: 20px; font-size: 20px;"><span class="icon-cog"></span></a>
									<ul>
										<li><a id="edit" style="padding-right: 10px;"><span class="icon-edit"></span>Редактировать</a></li>
									</ul>							
								</li>
							</ul>
						</nav>
						<section>							
						<table cellspacing="0" cellpadding="0" style="background: #E1E1E1; font: bold;">
							<thead><tr>
								<th class="center">Наименование</th>
								<th class="center">№ отказа</th>
								<th class="center">Дата</th>
							</tr></thead>
							<tbody>
								'.$rejection.'
							</tbody>
							
							</table>
						<table id="contractList" cellspacing="0" cellpadding="0" class="sortable">
							<thead><tr>
								<th class="center">Объект</th>
								<th class="center">Начало работ</th>
								<th class="center">Окончание работ</th>
								<th class="center">Плановая сумма</th>
								<th class="editor">Редактор</th>
							</tr></thead>
							<tbody>
								'.$t.'
							</tbody>
							</table>
                        </section>'
			);
			
		return json_encode($a);
	}
		

// Окно подтверждения удаления объекта 
// @input идентификатор объекта
	public static function deleteDialog($data){
		$r = parent::myquery("SELECT name FROM object WHERE id=".$data, PDO::FETCH_COLUMN);
		return "<div class='center' style='font-size: 20px;'>Удалить объект<br><br><strong>".$r[0]."</strong><br><br><span style='font-size: 30px'>?</span></div>";
	}
	
// Удаление объекта 
// @input идентификатор объекта
	public static function deleteAction($data){
		parent::dbConnect()->exec("DELETE FROM object_in_rejection WHERE id_object = '".$data[0]."'"); 
		parent::dbConnect()->exec("DELETE object,contragent_in_object FROM object, contragent_in_object WHERE object.id = contragent_in_object.id_object AND object.id = '".$data[0]."'"); 
		return self::ObjectInRejectionTable($data[1]);
	}
	
// Окно редактирования объекта из таблицы "Проекты"
// @input идентификатор объекта
	public static function editDialog($data){
		$dbh = parent::dbConnect();
		$r = $dbh->query("SELECT id_contragent FROM contragent_in_object WHERE id_object=".$data)->fetchAll(PDO::FETCH_COLUMN);
		$res = $dbh->query("SELECT id, name FROM contragent")->fetchAll(PDO::FETCH_OBJ);
			foreach($res as $v){
				if($r[0] == $v->id)
					$contragent .= '<option value="'.$v->id.'" selected>'.$v->name.'</option>';
				else
					$contragent .= '<option value="'.$v->id.'">'.$v->name.'</option>';
			}
		$object = $dbh->query("SELECT * FROM object WHERE id=".$data)->fetchAll(PDO::FETCH_OBJ);
		$res = $dbh->query("SELECT proguser_id, proguser_login FROM upravdoc.proguser")->fetchAll(PDO::FETCH_OBJ);
			foreach($res as $v){
				if($object[0]->bailee == $v->proguser_id)
					$personal .= '<option value="'.$v->proguser_id.'" selected>'.$v->proguser_login.'</option>';
				else
					$personal .= '<option value="'.$v->proguser_id.'">'.$v->proguser_login.'</option>';
			}
		$id_rejection = $dbh->query("SELECT id_rejection FROM object_in_rejection WHERE id_object=".$data)->fetchAll(PDO::FETCH_COLUMN);	
		
		$res = $dbh->query("SELECT id, name, number FROM contract")->fetchAll(PDO::FETCH_OBJ);
			foreach($res as $v){
					$contract .= '<option value="c_'.$v->id.'">'.$v->name.' '.$v->number.'</option>';
			}
			
		$res = $dbh->query("SELECT id, name, number FROM rejection")->fetchAll(PDO::FETCH_OBJ);
			foreach($res as $v){
				if ($v->id == $id_rejection[0])
					$rejection .= '<option value="r_'.$v->id.'" selected>'.$v->name.' '.$v->number.'</option>';
				else
					$rejection .= '<option value="r_'.$v->id.'">'.$v->name.' '.$v->number.'</option>';
			}
		return '
				<p>
					<input type="text" placeholder="Наименование объекта" id="object_name" class="col_12" value="'.htmlspecialchars($object[0]->name).'">
				</p>
				<p>
					<select id="object_contragent" data-placeholder="Заказчик" class="col_12">
						<option value="">Заказчик</option>
						'.$contragent.'
					</select>
				</p>
				<p>
					<input type="text" class="calendar_from col_6" placeholder="Начало работ" id="object_start_date" value="'.parent::ConvertDate($object[0]->start_date, "fromMySql").'">
					<input type="text" class="calendar_to col_6" placeholder="Окончание работ" id="object_end_date" value="'.parent::ConvertDate($object[0]->end_date, "fromMySql").'">
				</p>
				<p>
					<select id="object_personal" data-placeholder="Ответственный от СХС" class="col_12">
						<option value="">Ответственный от СХС</option>
						'.$personal.'
					</select>
				</p>
				<p>
					<input type="text" placeholder="Лимитная сумма" id="object_limit_sum" class="col_12" value="'.$object[0]->limit_sum.'">
				</p>
				<p>	
					<select id="object_contract" data-placeholder="Номер договора\отказа" class="col_12">
						<option value="">Номер договора\отказа</option>
						<option value="" style="background: #6DB9FF; font: bold;">Номер договора</option>
						'.$contract.'
						<option value="" style="background: #6DB9FF;">Номер отказа</option>
						'.$rejection.'
					</select>
				</p>
			';	
	}
	
//	Обновление объекта в базу данных
/* @input $data - Array(
						[0] => наименование объекта
						[1] => идентификатор контрагента
						[2] => дата начала дд.мм.гггг
						[3] => дата окончания дд.мм.гггг
						[4] => идентификатор ответственного лица
						[5] => лимитная сумма
						[6] => идентификатор договора(c_id) или отказа(r_id)
						[7] => идентификатор объекта
						[8] => id_rejection
					)
*/
// @return таблица ObjectInContractTable
	public static function editAction($data){
		$dbh = parent::dbConnect();
		$dbh->exec("UPDATE object SET name='".$data[0]."', start_date='".parent::ConvertDate($data[2], "toMySql")."', end_date='".parent::ConvertDate($data[3], "toMySql")."', limit_sum='".$data[5]."', bailee='".$data[4]."' WHERE id=".$data[7]);
		if($data[1] != null){
			$dbh->exec("UPDATE contragent_in_object SET id_contragent='".$data[1]."' WHERE id_object=".$data[7]);
		}
		if($data[6] != null){
			$d = explode('_', $data[6]);
			switch ($d[0]){
				case 'c': {
					$dbh->exec("DELETE FROM object_in_rejection WHERE id_object=".$data[7]);
					$dbh->exec("INSERT INTO object_in_contract VALUES ('','".$data[7]."','".$d[1]."')");
					$dbh->exec("UPDATE object SET connection='contract' WHERE id=".$data[7]);
					
				}
					break;
				case 'r': {
					$dbh->exec("DELETE FROM object_in_contract WHERE id_object=".$data[7]);
					$dbh->exec("INSERT INTO object_in_rejection VALUES ('','".$data[7]."','".$d[1]."')");
					$dbh->exec("UPDATE object SET connection='rejection' WHERE id=".$data[7]);
				}
					break;
			}	
		}
		return self::ObjectInRejectionTable($data[8]);
	}
}

?>