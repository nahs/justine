<?php
/****************************/
//	@system 		Justine
//	@description 	Контроллер страниц Материалы, Машины\Механизмы, Трудозатраты
//	@autor 			Nahs
//  @email 			nahs@bk.ru
//	@create			2013
//	@version		4.13
/****************************/
class MMWPage extends System{

// Пересылает на соответствующие страницы
/* @input $data = array(
					[0] => идентификатор работы
					[1] => страница (MMW-PBD)
					[2] => идентификатор объекта
					[3] => номер вкладки
				)
*/
// @return необходимую страницу
	public static function route($data){
		$d = array($data[0],$data[2], $data[3]);
		$page = explode('-',$data[1]);
		switch($page[0]){
			case 'materials':
				switch($page[1]){
					case 'plan': return self::router('MaterialsPlan', $d);break;
					case 'bid':	 return self::router('MaterialsBid', $d);break;
					case 'fact': return self::router('MaterialsFact', $d);break;
				};break;
			case 'machinery':
				switch($page[1]){
					case 'plan': return self::router('MachineryPlan', $d);break;
					case 'bid':  return self::router('MachineryBid', $d);break;
					case 'fact': return self::router('MachineryFact', $d);break;
				};break;
			case 'work':
				switch($page[1]){
					case 'plan': return self::router('WorkPlan', $d);break;
					case 'bid':  return self::router('WorkBid', $d);break;
					case 'fact': return self::router('WorkFact', $d);break;
				};break;
		}	
	}

// Подгружает необходимую страницу
// @input $class - имя класса	
/* @input $data = array(
					[0] => идентификатор работы
					[1] => страница (MMW-PBD)
					[2] => идентификатор объекта
				)
*/
// @return основная таблица страницы
	private static function router($class, $data){
		include_once('classes/'.$class.'Page.php');
		$method = $class.'Table';
		$class  = $class.'Page';
		return $class::$method( $data );
	}

}
