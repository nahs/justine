<?php
/****************************/
//	@system 		Justine
//	@description 	Системный контроллер
//	@autor 			Nahs
//  @email 			nahs@bk.ru
//	@create			2013
//	@version		4.13
/****************************/
class System{
	private static $DBTYPE	= 'mysql';
	private static $DBHOST	= '127.0.0.1';
	private static $DBNAME	= 'justine';
	private static $DBUSER	= 'mysql';
	private static $DBPASS	= 'mysql';
	private static $ERRORT	= 'ERROR:(';
	protected static $DOCS = '../docs/';
	protected static $TMP = '../tmp/';
	protected static $REPORT = '../report/';


	
//	функция коннектится к базе данных
//	@return линк объекта PDO
	protected static function dbConnect(){
		// создание объекта PDO и подключение к базе данных
	try{
		return new PDO(
						//'mysql:host=127.0.0.1;dbname=justine','mysql','mysql'
						self::$DBTYPE.":host=".
						self::$DBHOST.";dbname=".
						self::$DBNAME,
						self::$DBUSER,
						self::$DBPASS
					);
		}  
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}
	}

// обертка для сиквел-запроса
// @input $sql - сиквел-запрос
// @input $type - тип выгрузки данных
// @return данные из БД
	protected static function myquery($sql, $type = PDO::FETCH_OBJ){
		try{
			$res = self::dbConnect()->query($sql)->fetchAll($type);
		}
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}
		return $res;
	}
	
//	Конвертация даты из формата дд.мм.гггг в формат гггг-мм-дд
//	@input $date - конвертируемая дата
	protected static function ConvertDate($date,$type){
		switch ($type){
			case 'toMySql':	{
				$d = explode('.', $date);
				$r= $d[2].'-'.$d[1].'-'.$d[0];
			}
			break;
			case 'fromMySql':	{
				$d = explode('-', $date);
				$r = $d[2].'.'.$d[1].'.'.$d[0];
			}
			break;
		}
		return $r;
	}
	
	
//	выводит форму авторизации
//	@input	error - в случае ошибки подкрашивает поля красным
//	@input	log - логин юзера
//	@return	autorization form
	public static function authForm($error = null, $log = null){ 
		switch($error){
			case 'error':
				$errmess = '<p><strong>Доступ запрещен!</strong><p>';
				break;
			case null:
				$errmess = null;
				break;
		}
	
	
		$a = array( 
				'html' => '
						<div id="autorization">
							<p>'.$errmess.'</p>
							<p>
								<input id="login" placeholder="Пользователь" type="text" class="col_12" value="'.$log.'">
							</p>
							<p>
								<input id="passwd" placeholder="Пароль" type="password" class="col_12" value="">
							</p>
							<p>
								<button class="enterInSystem">Войти</button>
							</p>
						</div>'
		);
		
		return json_encode($a);
	}
	
//	Аутентификация пользователя	
//	@input 	logpas = array( 0 => login, 1 => password)
//	@return	таблицу ContractList
	public static function authAction($logpas){
		$log = md5($logpas[0]);
		$logpas[0] = md5($logpas[0]);
		try{
			$res = self::dbConnect()->prepare("SELECT name FROM personal WHERE login = ? AND passwd = ?");
			//$res = self::dbConnect()->prepare("SELECT proguser_login FROM upravdoc.proguser WHERE proguser_login = ? AND proguser_password = ?");
			$res->execute($logpas);
			$usr = $res->fetch(PDO::FETCH_OBJ);
		}
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}			
	 	if($usr != false){
			//$r = ProjectPage->ProjectListTable();
			$b = array(
				'fn' => 'auth',
				'fndata' => $log
			);
			//$a = json_decode($r, true);
			//$res = array_merge($a, $b);
			return json_encode($b);
		}
		else{
			return self::authForm( 'error', $logpas[0] ); 
			
		} 	
	}
	
	//	Уменьшение изображения	
	//	@input имя файла
	//	@return	уменьшенное изображение
	protected static function makeSmallImage( $filename, $final_width_of_image = 800 ){	
			
			if(preg_match('/[.](jpg|JPG)$/', $filename)) {
				$im = imagecreatefromjpeg(self::$DOCS.$filename);
			} //else die('incorrect format');
			/* if (preg_match('/[.](gif)$/', $filename)) {
				$im = imagecreatefromgif($path_to_image_directory.$filename);
			} else if (preg_match('/[.](png)$/', $filename)) {
				$im = imagecreatefrompng($path_to_image_directory.$filename);
			} */ //Определяем формат изображения
			
			$ox = imagesx($im);
			$oy = imagesy($im);
			$nx = $final_width_of_image;
			$ny = floor($oy * ($final_width_of_image / $ox));
			$nm = imagecreatetruecolor($nx, $ny);
			imagecopyresized($nm, $im, 0,0,0,0,$nx,$ny,$ox,$oy);
			imagejpeg($nm, self::$DOCS.$filename);
			
	}
	
	//	Шифрует изображения	
	//	@input имя файла
	//	@return	зашифрованное изображение	
	protected static function ImageCrypt($filename){
		$imgbinary = fread(fopen(self::$DOCS.$filename, "r"), filesize(self::$DOCS.$filename));
		$img_str = base64_encode($imgbinary);
		$img_dat = self::$DOCS.md5(basename($filename));
		file_put_contents($img_dat, $img_str);
		unlink(self::$DOCS.$filename);
	}
	
	//	расшифровывает изображения	
	//	@input имя файла
	//	@return	расшифрованное изображение	
	protected static function ImageDeCrypt($filename){
		copy(self::$DOCS.$filename, self::$TMP.$filename);
		$imgbinary = fread(fopen(self::$TMP.$filename, "r"), filesize(self::$TMP.$filename));
		$img_str = base64_decode($imgbinary);
		$img_dat = self::$TMP.$filename.'.jpg';
		file_put_contents($img_dat, $img_str);

	}

	//чистим папку tmp
	public static function clearTMP(){
		if($handle = opendir(self::$TMP))
		{
        while(false !== ($file = readdir($handle)))
                if($file != "." && $file != "..") unlink(self::$TMP.$file);
        closedir($handle);
		};
	}			

	//чистим папку report
	public static function clearReport(){
		if($handle = opendir(self::$REPORT))
		{
        while(false !== ($file = readdir($handle)))
                if($file != "." && $file != "..") unlink(self::$REPORT.$file);
        closedir($handle);
		};
	}			
	
	
	// Метод который ничего не делает
	public static function nothing(){}
	

}

?>