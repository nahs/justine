<?php
/****************************/
//	@system 		Justine
//	@description 	контроллер страницы "Вид работы"
//	@autor 			Nahs
//  @email 			nahs@bk.ru
//	@create			2013
//	@version		4.13
/****************************/
class TypeOfWorkPage extends System{
//	Таблица "Виды раьот"
//	@return	таблицу объектов
	public static function TypeOfWorkTable($data){
		$dbh = parent::dbConnect();
		$object = $dbh->query("SELECT name FROM object WHERE id=".$data)->fetchAll(PDO::FETCH_COLUMN);
		$res = $dbh->query("SELECT * FROM typeofwork WHERE id_object=".$data)->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
		$builder = $dbh->query("SELECT builder.name FROM builder LEFT JOIN builder_in_typeofwork ON builder.id = builder_in_typeofwork.id_builder WHERE builder_in_typeofwork.id_typeofwork =".$v->id)->fetchAll(PDO::FETCH_COLUMN);
		
		//плановая сумма
		$matplan = $dbh->query("SELECT amount, price FROM materialsplan WHERE worktype=".$v->id)->fetchAll(PDO::FETCH_OBJ);
		$mp_sum = 0;
		foreach($matplan as $mp){
			$mp_sum += $mp->amount*$mp->price;
		}
		$matplan = $dbh->query("SELECT amount, price FROM machineryplan WHERE worktype=".$v->id)->fetchAll(PDO::FETCH_OBJ);
		foreach($matplan as $mp){
			$mp_sum += $mp->amount*$mp->price;
		}
		$matplan = $dbh->query("SELECT amount, price FROM workplan WHERE worktype=".$v->id)->fetchAll(PDO::FETCH_OBJ);
		foreach($matplan as $mp){
			$mp_sum += $mp->amount*$mp->price;
		}

		//фактическая сумма
		$matpfact = $dbh->query("SELECT amount, price FROM materialsfact WHERE worktype=".$v->id)->fetchAll(PDO::FETCH_OBJ);
		$mf_sum = 0;
		foreach($matpfact as $mf){
			$mf_sum += $mf->amount*$mf->price;
		}
		$matpfact = $dbh->query("SELECT amount, price FROM machineryfact WHERE worktype=".$v->id)->fetchAll(PDO::FETCH_OBJ);
		foreach($matpfact as $mf){
			$mf_sum += $mf->amount*$mf->price;
		}
		$matpfact = $dbh->query("SELECT amount, price FROM workfact WHERE worktype=".$v->id)->fetchAll(PDO::FETCH_OBJ);
		foreach($matpfact as $mf){
			$mf_sum += $mf->amount*$mf->price;
		}

			switch($v->group){
				case '1': $g1 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>							
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>';
				break;
				case '2': $g2 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>							
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
				case '3': $g3 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>							
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
				case '4': $g4 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>							
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
				case '5': $g5 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>							
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
				case '6': $g6 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>							
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
				case '7': $g7 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>							
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
				case '8': $g8 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>													
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
				case '9': $g9 .= '<tr id="'.$v->id.'" class="line WorkList_tr">
					<td class="left" width="auto">'.$v->name.'</td>							
					<td class="left" width="auto">'.$builder[0].'</td>														
					<td class="left" width="100px">'.$mf_sum.'</td>							
					<td class="left" width="100px">'.$mp_sum.' руб.</td>							
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>'; break;
			}
		}
		$a = array(
			'html' => '<nav id="top-nav">
							<ul class="menu left">
								<li><a href="#project">Проекты</a></li>
								<li><a href="#сontract">Договоры</a></li>
								<li><a href="#rejection">Отказы</a></li>
								<li><a href="#handbook">Справочники</a></li>
								<li><a href="#report">Отчеты</a></li>
								<li class="adm"><a style="padding-right: 20px; font-size: 20px;"><span class="icon-cog"></span></a>
									<ul>
										<li><a id="newwork" style="padding-right: 10px;"><span class="icon-file"></span>Новая работа</a></li>
										<li><a id="edit" style="padding-right: 10px;"><span class="icon-edit"></span>Редактировать</a></li>
									</ul>							
								</li>
							</ul>
						</nav>
						<section>
						<h6 class="center object_name" id="'.$data.'">'.$object[0].'</h6>
						<div id="tabs">
							<ul class="tabs left">
								<li><a href="#глава1">Глава 1</a></li>
								<li><a href="#глава2">Глава 2</a></li>
								<li><a href="#глава3">Глава 3</a></li>
								<li><a href="#глава4">Глава 4</a></li>
								<li><a href="#глава5">Глава 5</a></li>
								<li><a href="#глава6">Глава 6</a></li>
								<li><a href="#глава7">Глава 7</a></li>
								<li><a href="#глава8">Глава 8</a></li>
								<li><a href="#глава9">Глава 9</a></li>
							</ul>
						
							<div id="глава1" class="tab-content">
								<table id="WorkListG1" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Подготовка территории строительства</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g1.'
								</tbody>
								</table>
							</div>
							<div id="глава2" class="tab-content">
								<table id="WorkListG2" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Основные объекты строительства</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g2.'
								</tbody>
								</table>
							</div>
							<div id="глава3" class="tab-content">
								<table id="WorkListG3" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Объекты подсобного и обслуживающего назначения</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g3.'
								</tbody>
								</table>
							</div>
							<div id="глава4" class="tab-content">
								<table id="WorkListG4" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Объекты энергетического хозяйства</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g4.'
								</tbody>
								</table>
							</div>
							<div id="глава5" class="tab-content">
								<table id="WorkListG5" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Объекты транспортного хозяйства и связи</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g5.'
								</tbody>
								</table>
							</div>
							<div id="глава6" class="tab-content">
								<table id="WorkListG6" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Наружные сети и сооружения водоснабжения, канализации, теплоснабжения и газоснабжения</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g6.'
								</tbody>
								</table>
							</div>
							<div id="глава7" class="tab-content">
								<table id="WorkListG7" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Благоустройство и озеленение территории</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g7.'
								</tbody>
								</table>
							</div>
							<div id="глава8" class="tab-content">
								<table id="WorkListG8" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Временные здания и соружения</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g8.'
								</tbody>
								</table>
							</div>
							<div id="глава9" class="tab-content">
								<table id="WorkListG9" cellspacing="0" cellpadding="0" class="sortable">
								<thead>	
								<tr>
									<th class="center" colspan="5">Прочие работы и затраты</th>
								</tr>
								<tr>
									<th class="center">Наименование работы</th>
									<th class="center">Подрядчик</th>
									<th class="center" width="80px">Фактическая сумма</th>
									<th class="center" width="80px">Плановая сумма</th>
									<th class="editor" width="80px">Редактор</th>
								</tr></thead>
								<tbody>
									'.$g9.'
								</tbody>
								</table>
							</div>
						</div>	
						</section>'
		);
		
		return json_encode($a);
	}
	
// форма для добавления нового объекта
	public static function AddNewDialog(){
		$dbh = parent::dbConnect();
		$res = $dbh->query("SELECT id, name FROM builder")->fetchAll(PDO::FETCH_OBJ);
		foreach($res as $v){
			$builder .= '<option value="'.$v->id.'">'.$v->name.'</option>';
		}
		
		return '
			<p>
				<input type="text" placeholder="Наименование работы" id="work_name" class="col_12">
			</p>
			<p>
				<select id="work_builder" data-placeholder="Подрядчик" class="col_12">
					<option value="">Подрядчик</option>
					'.$builder.'
				</select>
			</p>
			<p>
				<select id="work_type" data-placeholder="Вид работ" class="col_12">
					<option value="9">Вид работ</option>
					<option value="1">1. Подготовка территории строительства</option>
					<option value="2">2. Основные объекты строительства</option>
					<option value="3">3. Объекты подсобного и обслуживающего назначения</option>
					<option value="4">4. Объекты энергетического хозяйства</option>
					<option value="5">5. Объекты транспортного хозяйства и связи</option>
					<option value="6">6. Наружные сети и сооружения водоснабжения, канализации, теплоснабжения и газоснабжения</option>
					<option value="7">7. Благоустройство и озеленение территории</option>
					<option value="8">8. Временные здания и соружения</option>
					<option value="9">9. Прочие работы и затраты</option>
				</select>
			</p>
					';
	}

//	Запись новой работы в базу данных
/* @input $data - Array(
						[0] => наименование работы
						[1] => идентификатор подрядчика
						[2] => номер группы вида работ
						[3] => идентификатор работы
					)
*/
// @return таблица "Виды работ"
	public static function AddNewAction($data){
		$dbh = parent::dbConnect();
		$dbh->exec("INSERT INTO typeofwork VALUES ('', '".$data[0]."','".$data[2]."', '".$data[3]."')");
		$lastInsertId = $dbh->lastInsertId(); 
		if($data[1] != null){
			$dbh->exec("INSERT INTO builder_in_typeofwork VALUES ('','".$data[1]."','".$lastInsertId."')");
		} 
		return self::TypeOfWorkTable($data[3]);
	}
	
// Окно редактирования работы из таблицы "Тип работы"
// @input идентификатор работы
// @return таблица "Типы работ"
	public static function editDialog($data){
		$dbh = parent::dbConnect();
		$r = $dbh->query("SELECT id_builder FROM builder_in_typeofwork WHERE id_typeofwork=".$data)->fetchAll(PDO::FETCH_COLUMN);
		$res = $dbh->query("SELECT id, name FROM builder")->fetchAll(PDO::FETCH_OBJ);
			foreach($res as $v){
				if($r[0] == $v->id)
					$builder .= '<option value="'.$v->id.'" selected>'.$v->name.'</option>';
				else
					$builder .= '<option value="'.$v->id.'">'.$v->name.'</option>';
			}
		$work = $dbh->query("SELECT * FROM typeofwork WHERE id=".$data)->fetchAll(PDO::FETCH_OBJ);
		$wt[$work[0]->group] = "selected";
		return '
				<p>
					<input type="text" placeholder="Наименование работы" id="work_name" class="col_12" value="'.htmlspecialchars($work[0]->name).'">
				</p>
				<p>
					<select id="work_builder" data-placeholder="Подрядчик" class="col_12">
						<option value="">Подрядчик</option>
						'.$builder.'
					</select>
				</p>
				<p>
					<select id="work_type" data-placeholder="Вид работ" class="col_12">
						<option value="9">Вид работ</option>
						<option value="1" '.$wt[1].'>1. Подготовка территории строительства</option>
						<option value="2" '.$wt[2].'>2. Основные объекты строительства</option>
						<option value="3" '.$wt[3].'>3. Объекты подсобного и обслуживающего назначения</option>
						<option value="4" '.$wt[4].'>4. Объекты энергетического хозяйства</option>
						<option value="5" '.$wt[5].'>5. Объекты транспортного хозяйства и связи</option>
						<option value="6" '.$wt[6].'>6. Наружные сети и сооружения водоснабжения, канализации, теплоснабжения и газоснабжения</option>
						<option value="7" '.$wt[7].'>7. Благоустройство и озеленение территории</option>
						<option value="8" '.$wt[8].'>8. Временные здания и соружения</option>
						<option value="9" '.$wt[9].'>9. Прочие работы и затраты</option>
					</select>
				</p>
			';	
	}
	
//	Обновление работы в базу данных
/* @input $data - Array(
						[0] => наименование работы
						[1] => идентификатор подрядчика
						[2] => номер группы вида работ
						[3] => идентификатор работы
						[4] => идентификатор объекта
					)
*/
// @return таблица "Типы работ"
	public static function editAction($data){
		$dbh = parent::dbConnect();
		$dbh->exec("UPDATE `typeofwork` SET `name`='".$data[0]."', `group`=".$data[2]." WHERE `id`=".$data[3]);
		if($data[1] != null)
			if(!$dbh->exec("UPDATE builder_in_typeofwork SET id_builder='".$data[1]."' WHERE id_typeofwork=".$data[3]))
				$dbh->exec("INSERT INTO builder_in_typeofwork VALUES ('','".$data[1]."','".$data[3]."')");
			
		return self::TypeOfWorkTable($data[4]);
	}

// Окно подтверждения удаления работы
// @input идентификатор работы
	public static function deleteDialog($data){
		$r = parent::myquery("SELECT name FROM typeofwork WHERE id=".$data, PDO::FETCH_COLUMN);
		return "<div class='center' style='font-size: 20px;'>Удалить работу<br><br><strong>".$r[0]."</strong><br><br><span style='font-size: 30px'>?</span></div>";
	}
	
// Удаление работы
/* @input $data - Array(
						[0] => идентификатор работы
						[1] => идентификатор объекта
					)
*/
	public static function deleteAction($data){
		parent::dbConnect()->exec("DELETE typeofwork, builder_in_typeofwork FROM typeofwork, builder_in_typeofwork WHERE typeofwork.id = builder_in_typeofwork.id_typeofwork AND typeofwork.id = '".$data[0]."'"); 
		return self::TypeOfWorkTable($data[1]);
	}	

}

?>