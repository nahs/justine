<?
class View{

	private static $DBTYPE = 'mysql';
	private static $DBHOST = '127.0.0.1';
	private static $DBNAME = 'justine';
	private static $DBUSER = 'mysql';
	private static $DBPASS = 'mysql';
	private static $ERRORT = 'Прости, Хозяин.';
	
//	функция коннектится к базе данных
//	@return линк объекта PDO
	private static function dbConnect(){
		// создание объекта PDO и подключение к базе данных
	try{
		return new PDO(
						//'mysql:host=127.0.0.1;dbname=justine','mysql','mysql'
						self::$DBTYPE.":host=".
						self::$DBHOST.";dbname=".
						self::$DBNAME,
						self::$DBUSER,
						self::$DBPASS
					);
		}  
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}
	}

//	выводит форму авторизации
//	@input	error - в случае ошибки подкрашивает поля красным
//	@input	log - логин юзера
//	@return	autorization form
	public static function authForm($error = null, $log = null){ 
		switch($error){
			case 'error':
				$class = ' error';
				$errmess = '<p><strong>Доступ запрещен!</strong><p>';
				break;
			case null:
				$class = $errmess = null;
				break;
		}
	
		$a = array( 
				'html' => '<div id="autorization">
							<p>'.$errmess.'</p>
							<p>
								<input id="login" placeholder="Пользователь" type="text" class="col_10'.$class.'" value="'.$log.'">
							</p>
							<p>
								<input id="passwd" placeholder="Пароль" type="password" class="col_10'.$class.'" value="">
							</p>
							<p>
								<button class="enterInSystem">Войти</button>
							</p>
						</div>'
		);
		
		return json_encode($a);
	}
	
	
//	@input 	logpas = array( 0 => login, 1 => password)
//	@return	таблицу ContractList
	public static function authAction($logpas){
		$log = $logpas[0];
		$logpas[0] = md5($logpas[0]);
		try{
			$res = self::dbConnect()->prepare("SELECT name, access FROM personal WHERE login = ? AND passwd = ?");
			$res->execute($logpas);
			$usr = $res->fetch(PDO::FETCH_OBJ);
		}
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}			
	 	if($usr != false){
			$r = self::ProjectListTable($log);
			$b = array(
				'fn' => 'auth',
				'fndata' => $logpas[0]
			);
			$a = json_decode($r, true);
			$res = array_merge($a, $b);
			return json_encode($res);
		}
		else{
			return self::authForm( 'error', $log ); 
			
		} 	
	}

	
//	СПРАВОЧНИКИ------------------
	public static function HandbookPage(){
		$a = array( 
			'html'	=> '
			<nav id="top-nav">
				<ul class="menu center">
					<li><a href="#project">Проекты</a></li>
					<li><a href="#document">Документы</a></li>
					<li><a href="#personal">Сотрудники</a></li>
					<li><a href="#">Подразделения</a></li>
					<li><a href="#">Должности</a></li>
					<li><a href="#">Контрагенты</a></li>
					<li><a href="#">Субподрядчики</a></li>
					<li><a href="#">Транспортные средства</a></li>
					<li><a href="#">Материалы</a></li>
				</ul>
			</nav>
			<section></section>'
		);
			
		return json_encode($a);	
	}
	
	
//	Таблица "Список Проектов"
//	@input	user md5 login (для определения уровня доступа)
//	@return	table list
//25-26 февраля 79534842261
	public static function ProjectListTable($user){
		try{
			$res = self::dbConnect()->query("SELECT * FROM ProjectList")->fetchAll(PDO::FETCH_OBJ);
		}
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}	
			
		foreach($res as $v){
			$doc=$v->doc==1?'<span class="icon-file-alt" style="font-size: 20px;"></span>':'none';
			$t .= '<tr id="'.$v->id.'" class="line ProjectList_tr">
					<td class="left">'.$v->name.'</td>
					<td class="center" width="100px">'.$v->number.'</td>
					<td class="center" width="100px">'.$v->date.'</td>
					<td class="center tools doc">'.$doc.'</td>
					<td class="center tools editor">
						<span class="icon-edit" style="font-size: 20px;"></span>&nbsp;
						<span class="icon-trash" style="font-size: 20px;"></span>
					</td>
				</tr>';
				
		/* 	$docimg .= '<div id="docimg'.$v->id.'" class="docimg">
							<ul class="slideshow" width="600" height="800">
								<li class="center"><img src="/docs/1.jpg" width="600"/></li>
								<li class="center"><img src="/docs/2.jpg" width="600"/></li>
								<li class="center"><img src="/docs/3.jpg" width="600"/></li>
								<li class="center"><img src="/docs/4.jpg" width="600"/></li>
							</ul>
						</div>'; */
		}
		
		$a = array( 
			'html' 	=> '
						<nav id="top-nav">
							<ul class="menu left">
								<li><a href="#project">Проекты</a></li>
								<li><a href="#сontract">Договора</a></li>
								<li><a href="#rejection">Отказы</a></li>
								<li><a href="#handbook">Справочники</a></li>
								<li class="adm"><a style="padding-right: 20px; font-size: 20px;"><span class="icon-cog"></span></a>
									<ul>
										<li><a id="newproject" style="padding-right: 10px;"><span class="icon-file"></span>Новый объект</a></li>
										<li><a id="editproject" style="padding-right: 10px;"><span class="icon-edit"></span>Редактировать</a></li>
									</ul>							
								</li>
							</ul>
						</nav>
						<section>
							<table id="ProjectList" cellspacing="0" cellpadding="0" class="sortable">
							<thead><tr>
								<th style="width: auto;">Наименование объекта</th>
								<th style="width: 80px;">Заказчик</th>
								<th style="width: 80px;">Начало работ</th>
								<th style="width: 80px;">Окончание работ</th>
								<th style="width: 80px;">Плановая сумма</th>
								<th class="editor">Редактор</th>
							</tr></thead>
							<tbody>
								'.$t.'
							</tbody>
							</table>
						</section>'
			);
			
		return json_encode($a);
	}
	
// 	-------------------------------
//	@input id - array(0=>CLid, 1=>[ptf]-[mtr])

	public static function MTRTableView($id){
		$tid = explode("-",$id[1]);
		// CLid = '.$id[0].' 
		// ptf  = '.$tid[0].'
		// mtr  = '.$tid[1].'
		
		switch ($tid[1]){
			case 'm': 	$a['html'] = self::MaterialTable($id[0], $tid[0]); break;
			case 't':	$a['html'] = self::TransportTable($id[0], $tid[0]); break;
			case 'mm': 	$a['html'] = self::MechMachTable($id[0], $tid[0]); break;
			case 'tr': 	$a['html'] = self::LaborExpansesTable($id[0], $tid[0]); break;
		}
		
		$a['nav'] = '<ul class="menu">
								<li><a href="#document">Документы</a></li>
								<!-- <li><a href="#handbook">Справочники</a></li> -->
								<li class="adm"><a style="padding-right: 10px;"><span class="icon medium gray" data-icon="G"></span></a>
								<ul>
									<li><a id="newmtr" data-mtr="'.$id[0].'-'.$id[1].'" style="padding-right: 5px;"><span class="icon" data-icon="p"></span>Добавить</a></li>
									<li><a id="editmtr" data-mtr="'.$id[0].'-'.$id[1].'" style="padding-right: 10px;"><span class="icon" data-icon="7"></span>Редактировать</a></li>
								</ul>							
							</li>
						</ul>';
			
		return json_encode($a);
	}

//	--------------------------------
// @input	Clid - идентификатор договора
// @input	PTF - раздел
	private static function MaterialTable($CLid, $PTF){
		$CLname = self::dbConnect()->query("SELECT name FROM ContractList WHERE id=".$CLid)->fetch(PDO::FETCH_OBJ);
		switch ($PTF) {
			case 'plan':{
				$rows	= 'materials.plan_cost, materials.plan_amount';
				$thead 	= '	<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>План на материалы<br><br></td></tr>
							<tr><th style="width: auto;">Наименование</th>
							<th style="width: 80px;">Ед.изм.</th>
							<th style="width: 80px;">Кол-во</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Сумма</th></tr>';
			}
				break;
			case 'tender':{
				$rows = 'materials.tender_cost, materials.tender_amount, materials.tender_num, materials.tender_date'; 
				$thead 	= '	<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>Отпуск материалов<br><br></td></tr>
							<tr>
							<th style="width: auto;">Наименование</th>
							<th style="width: 80px;">Номер</th>
							<th style="width: 80px;">Дата</th>
							<th style="width: 80px;">Ед.изм.</th>
							<th style="width: 80px;">Кол-во</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Сумма</th></tr>';
			}
				break;
			case 'fact':{
				$rows = 'materials.fact_cost, materials.fact_amount, materials.fact_num, materials.fact_date, materials.fact_sender, materials.fact_recipient'; 
				$thead 	= '	
							<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>Факт по материалам<br><br></td></tr>
							<tr>
								<th style="width: auto;">Наименование</th>
								<th style="width: 80px;">Номер</th>
								<th style="width: 80px;">Дата</th>
								<th style="width: 80px;">Отправитель</th>
								<th style="width: 80px;">Получатель</th>
								<th style="width: 80px;">Ед.изм.</th>
								<th style="width: 80px;">Кол-во</th>
								<th style="width: 80px;">Стоимость</th>
								<th style="width: 80px;">Сумма</th>
							</tr>';
			}
				break;
		}
	
		try{
			$tbl = self::dbConnect()->query("SELECT materials.name, materials.unit, ".$rows." FROM materials WHERE materials.CLid = ".$CLid)->fetchAll(PDO::FETCH_OBJ);
		}
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}
		switch ($PTF) {
			case 'plan':
				foreach($tbl as $v){
					$t .= '<tr class="line">
							<td>'.$v->name.'</td>
							<td>'.$v->unit.'</td>
							<td>'.$v->plan_cost.'</td>
							<td>'.$v->plan_amount.'</td>
							<td>'.$v->plan_amount*$v->plan_cost.'</td>
						</tr>';
					}
				break;
			case 'tender':
				foreach($tbl as $v){
					$t .= '<tr class="line">
							<td>'.$v->name.'</td>
							<td>ОM '.$v->tender_num.'</td>
							<td>'.$v->tender_date.'</td>
							<td>'.$v->unit.'</td>
							<td>'.$v->tender_cost.'</td>
							<td>'.$v->tender_amount.'</td>
							<td>'.$v->tender_amount*$v->tender_cost.'</td>
						</tr>';
					}
				break;
			case 'fact':
				foreach($tbl as $v){
					$t .= '<tr class="line">
							<td>'.$v->name.'</td>
							<td>ФМ'.$v->fact_num.'</td>
							<td>'.$v->fact_date.'</td>
							<td>'.$v->fact_sender.'</td>
							<td>'.$v->fact_recipient.'</td>
							<td>'.$v->unit.'</td>
							<td>'.$v->fact_cost.'</td>
							<td>'.$v->fact_amount.'</td>
							<td>'.$v->fact_amount*$v->fact_cost.'</td>
						</tr>';
					}
				break;
		}
		
		return '<section>
					<table cellspacing="0" cellpadding="0" class="sortable">
					<thead><tr>
						'.$thead.'
					</tr></thead>
					<tbody>
						'.$t.'
					</tbody>
					</table>
				</section>'; 
	}	
	
	//By DK------------------------------------------------
//----------Вывод таблицы транспорт
private static function TransportTable($CLid, $PTF){
		$CLname = self::dbConnect()->query("SELECT name FROM ContractList WHERE id=".$CLid)->fetch(PDO::FETCH_OBJ);
		switch ($PTF) {
			case 'plan':{
				$rows	= 'transport.cost, transport.plan';
				$thead 	= '	<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>План на транспорт<br><br></td></tr>
							<tr><th style="width: auto;">Марка машины</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Плановые часы</th>
							<th style="width: 80px;">Сумма</th>';
			}
				break;
			case 'tender':{
				$rows = 'transport.number, transport.date, transport.cost, transport.tender'; 
				$thead 	= '	<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>Отпуск транспорта<br><br></td></tr>
							<tr><th style="width: auto;">Марка машины</th>
							<th style="width: 80px;">Номер документа</th>
							<th style="width: 80px;">Дата документа</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Часы работы</th>
							<th style="width: 80px;">Сумма</th>';
			}
				break;
			case 'fact':{
				$rows = 'transport.number, transport.date, transport.name_gruz, transport.gruz_pol, transport.gruz_otpr, transport.fact, transport.cost'; 
				$thead 	= '	
							<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>Факт по транспорту<br><br></td></tr>
							<tr><th style="width: auto;">Марка машины</th>
							<th style="width: 80px;">Номер документа</th>
							<th style="width: 80px;">Дата документа</th>
							<th style="width: 80px;">Название груза</th>
							<th style="width: 80px;">Грузополучатель</th>
							<th style="width: 80px;">Грузоотправитель</th>
							<th style="width: 80px;">Часы работы</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Сумма</th>';
			}
				break;
		}
	
		try{
			$tbl = self::dbConnect()->query("SELECT transport.name, ".$rows." FROM transport WHERE transport.CLid = ".$CLid)->fetchAll(PDO::FETCH_OBJ);
		}
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}
		switch ($PTF) {
			case 'plan':
				foreach($tbl as $v){
					$t .= '<tr class="line">
							<td>'.$v->name.'</td>
							<td>'.$v->cost.'</td>
							<td>'.$v->plan.'</td>
							<td>'.$v->plan*$v->cost.'</td>
						</tr>';
					}
				break;
			case 'tender':
				foreach($tbl as $v){
					$t .= '<tr class="line"> 
							<td>'.$v->name.'</td>
							<td>'.$v->numer.'</td>
							<td>'.$v->date.'</td>
							<td>'.$v->cost.'</td>
							<td>'.$v->tender.'</td>
							<td>'.$v->tender*$v->cost.'</td>
						</tr>';
					}
				break;
			case 'fact':
				foreach($tbl as $v){
					$t .= '<tr class="line"> 
							<td>'.$v->name.'</td>
							<td>'.$v->numer.'</td>
							<td>'.$v->date.'</td>
							<td>'.$v->name_gruz.'</td>
							<td>'.$v->gruz_pol.'</td>
							<td>'.$v->gruz_otpr.'</td>
							<td>'.$v->cost.'</td>
							<td>'.$v->fact.'</td>
							<td>'.$v->fact*$v->cost.'</td>
						</tr>';
					}
				break;
		}
		
		return '<section>
					<table cellspacing="0" cellpadding="0" class="sortable">
					<thead><tr>
						'.$thead.'
					</tr></thead>
					<tbody>
						'.$t.'
					</tbody>
					</table>
				</section>'; 
	}


//----------Вывод таблицы машины/механизмы
private static function MechMachTable($CLid, $PTF){
		$CLname = self::dbConnect()->query("SELECT name FROM ContractList WHERE id=".$CLid)->fetch(PDO::FETCH_OBJ);
		switch ($PTF) {
			case 'plan':{
				$rows	= 'mashmex.cost, mashmex.plan';
				$thead 	= '	<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>План на машины\\механизмы<br><br></td></tr>
							<tr><th style="width: auto;">Марка машины</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Плановые часы</th>
							<th style="width: 80px;">Сумма</th>';
			}
				break;
			case 'tender':{
				$rows = 'mashmex.number, mashmex.date, mashmex.cost, mashmex.tender'; 
				$thead 	= '	<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>Отпуск машин\\механизмов<br><br></td></tr>
							<tr><th style="width: auto;">Марка машины</th>
							<th style="width: 80px;">Номер документа</th>
							<th style="width: 80px;">Дата документа</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Часы работы</th>
							<th style="width: 80px;">Сумма</th>';
			}
				break;
			case 'fact':{
				$rows = 'mashmex.number, mashmex.date, mashmex.name_gruz, mashmex.gruz_pol, mashmex.gruz_otpr, mashmex.fact, mashmex.cost'; 
				$thead 	= '	
							<tr><td colspan="9" class="tabhead">'.$CLname->name.'<hr>Факт по машинам\\механизмов<br><br></td></tr>
							<tr><th style="width: auto;">Марка машины</th>
							<th style="width: 80px;">Номер документа</th>
							<th style="width: 80px;">Дата документа</th>
							<th style="width: 80px;">Название груза</th>
							<th style="width: 80px;">Грузополучатель</th>
							<th style="width: 80px;">Грузоотправитель</th>
							<th style="width: 80px;">Часы работы</th>
							<th style="width: 80px;">Стоимость</th>
							<th style="width: 80px;">Сумма</th>';
			}
				break;
		}
	
		try{
			$tbl = self::dbConnect()->query("SELECT mashmex.name, ".$rows." FROM mashmex WHERE mashmex.CLid = ".$CLid)->fetchAll(PDO::FETCH_OBJ);
		}
		catch(PDOException $e) {  
			file_put_contents('error/PDOErrors.txt', date("d.m.Y H:i:s").' '.$e->getMessage(), FILE_APPEND); 
			exit('<div class="notice error">'.self::$ERRORT.'</div>');
		}
		switch ($PTF) {
			case 'plan':
				foreach($tbl as $v){
					$t .= '<tr class="line">
							<td>'.$v->name.'</td>
							<td>'.$v->cost.'</td>
							<td>'.$v->plan.'</td>
							<td>'.$v->plan*$v->cost.'</td>
						</tr>';
					}
				break;
			case 'tender':
				foreach($tbl as $v){
					$t .= '<tr class="line"> 
							<td>'.$v->name.'</td>
							<td>'.$v->numer.'</td>
							<td>'.$v->date.'</td>
							<td>'.$v->cost.'</td>
							<td>'.$v->tender.'</td>
							<td>'.$v->tender*$v->cost.'</td>
						</tr>';
					}
				break;
			case 'fact':
				foreach($tbl as $v){
					$t .= '<tr class="line"> 
							<td>'.$v->name.'</td>
							<td>'.$v->numer.'</td>
							<td>'.$v->date.'</td>
							<td>'.$v->name_gruz.'</td>
							<td>'.$v->gruz_pol.'</td>
							<td>'.$v->gruz_otpr.'</td>
							<td>'.$v->cost.'</td>
							<td>'.$v->fact.'</td>
							<td>'.$v->fact*$v->cost.'</td>
						</tr>';
					}
				break;
		}
		
		return '<section>
					<table cellspacing="0" cellpadding="0" class="sortable">
					<thead><tr>
						'.$thead.'
					</tr></thead>
					<tbody>
						'.$t.'
					</tbody>
					</table>
				</section>'; 
	}

//--------------------------------------------------	

//	форма для добавления нового диалога MTR
	public static function newMTRDialog($data){
		$tid = explode("-",$data);
		//$tid[0] - Clid
		//$tid[1] - ptf
		//$tid[2] - mtr
		
		switch ($tid[2]){
			case 'm': 	$a = self::MaterialAddDialog($tid[0], $tid[1]); break;
			case 't':	$a = self::TransportAddDialog($tid[0], $tid[1]); break;
			case 'mm': 	$a = self::MechMachAddDialog($tid[0], $tid[1]); break;
			case 'tr': 	$a = self::LaborExpansesAddDialog($tid[0], $tid[1]); break;
		}

		return $a;
	}
// форма для добавления нового поля CL
	public static function newProjectDialog(){
		return '
			<input type="text" placeholder="Наименование" id="CLname" class="col_11"><br>	
			<input type="text" placeholder="Номер" id="CLnum" class="col_5"><br>
			<input type="text" placeholder="Дата" id="CLdate" class="col_5"><br>
			<input type="file" id="CLdoc" class="col_11">
		';
	}

// форма для добавления нового поля materials
	public static function MaterialAddDialog($CLid, $PTR){
		switch ($PTF) {
			case 'plan':
				return '
					<input type="text" placeholder="Наименование" id="Mname" class="col_11"><br>	
					<input type="text" placeholder="Ед. измерения" id="Munit" class="col_5"><br>
					<input type="text" placeholder="Количество" id="Mamount" class="col_5"><br>
					<input type="text" placeholder="Стоимость" id="Mcost" class="col_5"><br>
				';
			break;
			case 'tender':break;
			case 'fact':break;
		}
	}
	
// 	Добавление нового договара в базу данных
//	@input - data = array(CLname, CLnum, CLdate)
//	@return - table list
	public static function AddNewContract($data){
		$STH = self::dbConnect()->prepare("INSERT INTO ContractList (name,number,date) VALUES (?, ?, ?)")->execute($data);
		
		return self::ProjecttListTable();
	}
	
// Форма спрашивающая подтверждение удаления поля
// @input $CLid
// @return - CL table
	public static function deleteContractDialog($CLid){
		$CLname = self::dbConnect()->query("SELECT name FROM ContractList WHERE id=".$CLid)->fetch(PDO::FETCH_OBJ);
		
		return '<p>Вы действительно хотите удалить договор<br>&quot;'.$CLname->name.'&quot;?</p>';
	}

// TODO:  удалить строки с заданным значением CLid из всех таблиц	
// 	Удаление Договора из всех таблиц
//	@input CLid
//	@return CL table
	public static function deleteContract($CLid){
		self::dbConnect()->exec("DELETE FROM ContractList WHERE id=".$CLid);
		return self::ProjecttListTable();
	}
	
// Форма редактрования договора
//	@input CLid
//	@return 
	public static function editContractDialog($CLid){
		$CL = self::dbConnect()->query("SELECT name, number, date FROM ContractList WHERE id=".$CLid)->fetch(PDO::FETCH_OBJ);
		
		return '
			<input type="text" placeholder="Наименование" id="CLname" class="col_11" value="'.$CL->name.'"><br>	
			<input type="text" placeholder="Номер" id="CLnum" class="col_5" value="'.$CL->number.'"><br>
			<input type="text" placeholder="Дата" id="CLdate" class="col_5" value="'.$CL->date.'"><br>
			<!--<input type="file" id="CLdoc" class="col_11">-->
		';
	}
	
	
// 	Обновления строки Договора
//	@input - data = array(CLname, CLnum, CLdate, CLid)
//	@return - table list
	public static function SaveEditingContract($data){
		self::dbConnect()->exec("UPDATE ContractList SET name = '".$data[0]."', number = '".$data[1]."', date = ".$data[2]." WHERE id=".$data[3]."");
		return self::ProjecttListTable();
	}
}
?>